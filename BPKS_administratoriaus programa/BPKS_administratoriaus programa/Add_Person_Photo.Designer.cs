﻿namespace BPKS_administratoriaus_programa
{
    partial class Add_Person_Photo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.face_PictureBox = new System.Windows.Forms.PictureBox();
            this.image_PictureBox = new System.Windows.Forms.PictureBox();
            this.save_Button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.add_Button = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label_done = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label_req = new System.Windows.Forms.Label();
            this.label_images_count = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.face_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.image_PictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.button1.Location = new System.Drawing.Point(864, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 37);
            this.button1.TabIndex = 16;
            this.button1.Text = "Išeiti";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // face_PictureBox
            // 
            this.face_PictureBox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.face_PictureBox.Location = new System.Drawing.Point(42, 95);
            this.face_PictureBox.Name = "face_PictureBox";
            this.face_PictureBox.Size = new System.Drawing.Size(150, 150);
            this.face_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.face_PictureBox.TabIndex = 4;
            this.face_PictureBox.TabStop = false;
            // 
            // image_PictureBox
            // 
            this.image_PictureBox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.image_PictureBox.Location = new System.Drawing.Point(12, 44);
            this.image_PictureBox.Name = "image_PictureBox";
            this.image_PictureBox.Size = new System.Drawing.Size(438, 300);
            this.image_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.image_PictureBox.TabIndex = 3;
            this.image_PictureBox.TabStop = false;
            // 
            // save_Button
            // 
            this.save_Button.Location = new System.Drawing.Point(864, 80);
            this.save_Button.Name = "save_Button";
            this.save_Button.Size = new System.Drawing.Size(94, 37);
            this.save_Button.TabIndex = 17;
            this.save_Button.Text = "Iššaugoti";
            this.save_Button.UseVisualStyleBackColor = true;
            this.save_Button.Click += new System.EventHandler(this.save_Button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Asmens ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label4.Location = new System.Drawing.Point(67, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Aptiktas veido atvaizdas";
            // 
            // add_Button
            // 
            this.add_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.add_Button.Location = new System.Drawing.Point(61, 260);
            this.add_Button.Name = "add_Button";
            this.add_Button.Size = new System.Drawing.Size(93, 39);
            this.add_Button.TabIndex = 21;
            this.add_Button.Text = "Pridėti";
            this.add_Button.UseVisualStyleBackColor = true;
            this.add_Button.Click += new System.EventHandler(this.add_Button_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(61, 19);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(277, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 22;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label_done);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label_req);
            this.groupBox1.Controls.Add(this.label_images_count);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.face_PictureBox);
            this.groupBox1.Controls.Add(this.add_Button);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(471, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(387, 367);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Veido atvaizdų pridėjimas";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(252, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Paskutinis pridėtas veidas";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pictureBox1.Location = new System.Drawing.Point(262, 145);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // label_done
            // 
            this.label_done.AutoSize = true;
            this.label_done.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_done.ForeColor = System.Drawing.Color.SeaGreen;
            this.label_done.Location = new System.Drawing.Point(43, 315);
            this.label_done.Name = "label_done";
            this.label_done.Size = new System.Drawing.Size(295, 24);
            this.label_done.TabIndex = 27;
            this.label_done.Text = "Veido atvaizdai BPKS paruošti!";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(198, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "iš";
            // 
            // label_req
            // 
            this.label_req.AutoSize = true;
            this.label_req.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_req.Location = new System.Drawing.Point(218, 55);
            this.label_req.Name = "label_req";
            this.label_req.Size = new System.Drawing.Size(21, 13);
            this.label_req.TabIndex = 25;
            this.label_req.Text = "10";
            // 
            // label_images_count
            // 
            this.label_images_count.AutoSize = true;
            this.label_images_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_images_count.Location = new System.Drawing.Point(178, 55);
            this.label_images_count.Name = "label_images_count";
            this.label_images_count.Size = new System.Drawing.Size(14, 13);
            this.label_images_count.TabIndex = 24;
            this.label_images_count.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Pridėta atvaizdų:";
            // 
            // Add_Person_Photo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 398);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.save_Button);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.image_PictureBox);
            this.Name = "Add_Person_Photo";
            this.Text = "Asmenų veido atvaizdų ruošimas";
            ((System.ComponentModel.ISupportInitialize)(this.face_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.image_PictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox image_PictureBox;
        private System.Windows.Forms.PictureBox face_PictureBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button save_Button;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button add_Button;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_req;
        private System.Windows.Forms.Label label_images_count;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label_done;
        private System.Windows.Forms.Label label6;
    }
}