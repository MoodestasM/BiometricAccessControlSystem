﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Xml;
using System.Threading;
using System.Drawing;
using MySql.Data.MySqlClient;
// Emgu library
using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

namespace BPKS_administratoriaus_programa
{
    public partial class Add_Person_Photo : Form
    {
        #region Variables
        //This is my connection string i have assigned the database file address path
        string MyConnection = "datasource=127.0.0.1;port=3306;username=root;password=Modestas";

        //Camera specific
        Capture grabber;

        //Images for finding face
        Image<Bgr, Byte> currentFrame;
        Image<Gray, byte> result = null;
        Image<Gray, byte> gray_frame = null;

        //Haar Cascade Classifier 
        public CascadeClassifier Face = new CascadeClassifier(Application.StartupPath + "/HaarCascades/haarcascade_frontalface_default.xml");

        //Variables for face set
        public List<Image<Gray, byte>> resultImages = new List<Image<Gray, byte>>();
        int req_Face_Number = 10;
        int face_Number = 0;
        int progressPercentage = 0;

        //Byte array for converted images
        byte[][] aquired_images = new byte[10][];
        #endregion

        public Add_Person_Photo(string asmID)
        {
            InitializeComponent();
            save_Button.Enabled = false;
            label4.Text = asmID;
            label_req.Text = req_Face_Number.ToString();
            label_done.Visible = false;
            SetMyButtonProperties();
            //Disable form resizing for users 
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            //Start capture
            initialise_capture();
        }
        //Set button properties
        private void SetMyButtonProperties()
        {
            //Assign an image to the button.
            button1.Image = Image.FromFile(Application.StartupPath + "/Resources/14309203241732.png");
            save_Button.Image = Image.FromFile(Application.StartupPath + "/Resources/1430929416_678134-sign-check-32.png");
            add_Button.Image = Image.FromFile(Application.StartupPath + "/Resources/1430949482_678092-sign-add-32.png");
            //Align the image and text on the button.
            button1.ImageAlign = ContentAlignment.MiddleRight;
            button1.TextAlign = ContentAlignment.MiddleLeft;
            add_Button.TextAlign = ContentAlignment.MiddleLeft;
            add_Button.ImageAlign = ContentAlignment.MiddleRight;
            save_Button.ImageAlign = ContentAlignment.MiddleRight;
            save_Button.TextAlign = ContentAlignment.MiddleLeft;
        }
        //Camera capture start
        public void initialise_capture()
        {
            grabber = new Capture();
            grabber.QueryFrame();
            //Initialize the FrameGraber event
            Application.Idle += new EventHandler(FrameGrabber);
        }
        //Camera capture stop
        private void stop_capture()
        {
            Application.Idle -= new EventHandler(FrameGrabber);
            if (grabber != null)
            {
                grabber.Dispose();
            }
            //Initialize the FrameGraber event
        }
        //Process Frame
        void FrameGrabber(object sender, EventArgs e)
        {
            //Get the current frame form capture device
            currentFrame = grabber.QueryFrame().Resize(320, 240, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

            //Convert capture to Grayscale
            if (currentFrame != null)
            {
                gray_frame = currentFrame.Convert<Gray, Byte>();

                //Face Detector
                Rectangle[] facesDetected = Face.DetectMultiScale(gray_frame, 1.2, 10, new Size(140, 140), Size.Empty);

                //Action for each element detected
                for (int i = 0; i < facesDetected.Length; i++)// (Rectangle face_found in facesDetected)
                {
                    //This will focus in on the face from the haar results its not perfect but it will remove a majoriy
                    //of the background noise
                    facesDetected[i].X += (int)(facesDetected[i].Height * 0.15);
                    facesDetected[i].Y += (int)(facesDetected[i].Width * 0.22);
                    facesDetected[i].Height -= (int)(facesDetected[i].Height * 0.3);
                    facesDetected[i].Width -= (int)(facesDetected[i].Width * 0.35);

                    result = currentFrame.Copy(facesDetected[i]).Convert<Gray, byte>().Resize(140, 140, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                    result._EqualizeHist();
                    face_PictureBox.Image = result.ToBitmap();
                    //Draw the face detected in the 0th (gray) channel with blue color
                    currentFrame.Draw(facesDetected[i], new Bgr(Color.Red), 2);

                }
                image_PictureBox.Image = currentFrame.ToBitmap();
            }
        }

        //Cancel
        private void button1_Click(object sender, EventArgs e)
        {
            //Cancel this operation
            Application.Idle -= new EventHandler(FrameGrabber);
            if (grabber != null)
            {
                grabber.Dispose();
            }
            //Initialize the FrameGraber event
            this.Close();
        }

        private void save_Button_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < resultImages.Count; i++)
            {
                try
                {
                    //Using MemoryStream
                    using (MemoryStream ms = new MemoryStream())
                    {
                        //Convert Image to byte[]
                        resultImages[i].Bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                        byte[] imageBytes = ms.ToArray();

                        aquired_images[i] = imageBytes;
                    }
                    //This is my insert query in which i am taking input from the user through windows forms
                    string Query = "insert into bpks.asm_nuotraukos(Asm_ID,BPKS_Nuotrauka) values('" + this.label4.Text + "',@IMG);";
                    //This is  MySqlConnection here i have created the object and pass my connection string.
                    MySqlConnection MyConn = new MySqlConnection(MyConnection);
                    //This is command class which will handle the query and connection object.
                    MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyCommand.Parameters.Add(new MySqlParameter("IMG", aquired_images[i]));
                    MyReader = MyCommand.ExecuteReader();     //Here our query will be executed and data saved into the database.
                    while (MyReader.Read())
                    {
                    }
                    MyConn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            //Stop FrameGrabber
            stop_capture();
            resultImages.Clear();
            //Return to the main form
            // Form1 TF = new Form1();
            // TF.return_From_Add_Person();
            //
            //An instance of already opened form and call the method from there
            if (System.Windows.Forms.Application.OpenForms["Form1"] != null)
            {
                (System.Windows.Forms.Application.OpenForms["Form1"] as Form1).return_From_Add_Person();
            }
            //Close this frame
            this.Close();
        }

        private void add_Button_Click(object sender, EventArgs e)
        {
           stop_capture();
            //Check or picture box not null
           if (face_PictureBox.Image != null)
           {
               //Display progress
               face_Number = face_Number + 1;
               label_images_count.Text = face_Number.ToString();
               progressPercentage = face_Number * 10;
               progressBar1.Value = progressPercentage;
               //Add detected face to object
               resultImages.Add(result);
               pictureBox1.Image = resultImages[face_Number - 1].ToBitmap();
           }
           else
               MessageBox.Show("Nėra aptiktų veidų!", "Klaida");
           initialise_capture();
           //Check or need more faces
           if (face_Number == req_Face_Number)
           {
            // if done
            add_Button.Enabled = false;
            save_Button.Enabled = true;
            label_done.Visible = true;
           }
         }

    }
}

