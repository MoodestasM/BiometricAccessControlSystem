﻿namespace BPKS_administratoriaus_programa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.addPerson = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.dataManagement = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.watch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.register = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.about = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox_image_path = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.next_button = new System.Windows.Forms.Button();
            this.prev_button = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.toPersonAddForm = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.button_Refresh = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.neat_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.neat_nuotrauka = new System.Windows.Forms.DataGridViewImageColumn();
            this.Laikas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pral_Punktas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ein_Asm_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Pavarde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Pareigos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Darbo_Vieta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Laikas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Pral_Punktas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Tel_Nr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_El_pastas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ein_Darb_Nuotrauka = new System.Windows.Forms.DataGridViewImageColumn();
            this.ein_Nuotrauka = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button_PDF = new System.Windows.Forms.Button();
            this.button_Excel = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label_SelPerson = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.reg_Asm_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Pavarde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Pareigos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Darbo_Vieta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Atejo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Isejo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Darbo_Laikas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Pral_Punktas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Tel_Nr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_El_pastas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_Nuotrauka = new System.Windows.Forms.DataGridViewImageColumn();
            this.reg_BPKS_Nuotrauka1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.reg_BPKS_Nuotrauka2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.reg_Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_DeletePerson = new System.Windows.Forms.Button();
            this.button_Save_Updated_Data = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox_Browse_File = new System.Windows.Forms.TextBox();
            this.Button_Browse_File = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.text_Edit_ID = new System.Windows.Forms.TextBox();
            this.text_Edit_Workplace = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.text_Edit_Position = new System.Windows.Forms.TextBox();
            this.text_Edit_Email = new System.Windows.Forms.TextBox();
            this.text_Edit_Phone = new System.Windows.Forms.TextBox();
            this.text_Edit_Surname = new System.Windows.Forms.TextBox();
            this.text_Edit_Name = new System.Windows.Forms.TextBox();
            this.clearSearchField = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Asm_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pavarde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tel_Nr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.El_pastas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pareigos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Darbo_Vieta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Darb_Nuotrauka = new System.Windows.Forms.DataGridViewImageColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1337, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPerson,
            this.toolStripSeparator1,
            this.dataManagement,
            this.toolStripSeparator2,
            this.watch,
            this.toolStripSeparator3,
            this.register,
            this.toolStripSeparator4,
            this.about});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1337, 55);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // addPerson
            // 
            this.addPerson.Image = ((System.Drawing.Image)(resources.GetObject("addPerson.Image")));
            this.addPerson.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addPerson.Name = "addPerson";
            this.addPerson.Size = new System.Drawing.Size(134, 52);
            this.addPerson.Text = "Pridėti asmenį";
            this.addPerson.ToolTipText = "Pridėti naują asmenį";
            this.addPerson.Click += new System.EventHandler(this.addPerson_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 55);
            // 
            // dataManagement
            // 
            this.dataManagement.Image = ((System.Drawing.Image)(resources.GetObject("dataManagement.Image")));
            this.dataManagement.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dataManagement.Name = "dataManagement";
            this.dataManagement.Size = new System.Drawing.Size(165, 52);
            this.dataManagement.Text = "Duomenų valdymas";
            this.dataManagement.Click += new System.EventHandler(this.dataManagement_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 55);
            // 
            // watch
            // 
            this.watch.Image = ((System.Drawing.Image)(resources.GetObject("watch.Image")));
            this.watch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.watch.Name = "watch";
            this.watch.Size = new System.Drawing.Size(116, 52);
            this.watch.Text = "Stebėjimas";
            this.watch.Click += new System.EventHandler(this.watch_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 55);
            // 
            // register
            // 
            this.register.Image = ((System.Drawing.Image)(resources.GetObject("register.Image")));
            this.register.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(106, 52);
            this.register.Text = "Registras";
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 55);
            // 
            // about
            // 
            this.about.Image = ((System.Drawing.Image)(resources.GetObject("about.Image")));
            this.about.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(83, 52);
            this.about.Text = "Apie";
            this.about.ToolTipText = "Apie";
            this.about.Click += new System.EventHandler(this.about_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(11, 89);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1269, 496);
            this.panel1.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.textBox_image_path);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Location = new System.Drawing.Point(503, 72);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(239, 343);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Asmens nuotrauka";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(17, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(197, 248);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // textBox_image_path
            // 
            this.textBox_image_path.Location = new System.Drawing.Point(17, 294);
            this.textBox_image_path.Name = "textBox_image_path";
            this.textBox_image_path.Size = new System.Drawing.Size(126, 20);
            this.textBox_image_path.TabIndex = 19;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(149, 291);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(65, 25);
            this.button4.TabIndex = 8;
            this.button4.Text = "Naršyti..";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.next_button);
            this.groupBox2.Controls.Add(this.prev_button);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.toPersonAddForm);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.groupBox2.Location = new System.Drawing.Point(36, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(402, 403);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "BPKS veido atvaizdai";
            // 
            // next_button
            // 
            this.next_button.Location = new System.Drawing.Point(274, 347);
            this.next_button.Name = "next_button";
            this.next_button.Size = new System.Drawing.Size(75, 23);
            this.next_button.TabIndex = 6;
            this.next_button.Text = ">>";
            this.next_button.UseVisualStyleBackColor = true;
            this.next_button.Click += new System.EventHandler(this.next_button_Click);
            // 
            // prev_button
            // 
            this.prev_button.Location = new System.Drawing.Point(47, 344);
            this.prev_button.Name = "prev_button";
            this.prev_button.Size = new System.Drawing.Size(75, 23);
            this.prev_button.TabIndex = 5;
            this.prev_button.Text = "<<";
            this.prev_button.UseVisualStyleBackColor = true;
            this.prev_button.Click += new System.EventHandler(this.prev_button_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pictureBox2.Location = new System.Drawing.Point(86, 118);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(200, 200);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // toPersonAddForm
            // 
            this.toPersonAddForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.toPersonAddForm.Location = new System.Drawing.Point(30, 43);
            this.toPersonAddForm.Name = "toPersonAddForm";
            this.toPersonAddForm.Size = new System.Drawing.Size(256, 42);
            this.toPersonAddForm.TabIndex = 3;
            this.toPersonAddForm.Text = "Ruošti veido atvaizdus BPKS";
            this.toPersonAddForm.UseVisualStyleBackColor = true;
            this.toPersonAddForm.Click += new System.EventHandler(this.toPersonAddForm_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.button2.Location = new System.Drawing.Point(1026, 26);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 40);
            this.button2.TabIndex = 5;
            this.button2.Text = "Išsaugoti asmenį";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.groupBox1.Location = new System.Drawing.Point(800, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(430, 343);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Asmens informacija";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(255, 75);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(72, 23);
            this.button7.TabIndex = 18;
            this.button7.Text = "Generuoti";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox7.Location = new System.Drawing.Point(135, 268);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(192, 21);
            this.textBox7.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label7.Location = new System.Drawing.Point(35, 268);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "Darbo vieta";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label6.Location = new System.Drawing.Point(35, 237);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "Pareigos";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(331, 20);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(81, 26);
            this.button3.TabIndex = 6;
            this.button3.Text = "Išvalyti";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label5.Location = new System.Drawing.Point(35, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 16);
            this.label5.TabIndex = 14;
            this.label5.Text = "El. Paštas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label4.Location = new System.Drawing.Point(33, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Telefono nr.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label3.Location = new System.Drawing.Point(35, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Pavardė";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label2.Location = new System.Drawing.Point(35, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Vardas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(35, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Asmens ID";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox6.Location = new System.Drawing.Point(135, 237);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(192, 21);
            this.textBox6.TabIndex = 12;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox5.Location = new System.Drawing.Point(135, 203);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(192, 21);
            this.textBox5.TabIndex = 13;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox4.Location = new System.Drawing.Point(135, 174);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(192, 21);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox3.Location = new System.Drawing.Point(135, 141);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(192, 21);
            this.textBox3.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox2.Location = new System.Drawing.Point(135, 108);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(192, 21);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox1.Location = new System.Drawing.Point(135, 75);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(113, 21);
            this.textBox1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBox9);
            this.panel3.Controls.Add(this.button_Refresh);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.groupBox7);
            this.panel3.Controls.Add(this.groupBox6);
            this.panel3.Location = new System.Drawing.Point(12, 84);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1310, 510);
            this.panel3.TabIndex = 0;
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox9.Location = new System.Drawing.Point(86, 9);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(307, 26);
            this.textBox9.TabIndex = 7;
            this.textBox9.TextChanged += new System.EventHandler(this.watch_UnCellClick);
            // 
            // button_Refresh
            // 
            this.button_Refresh.Location = new System.Drawing.Point(1226, 9);
            this.button_Refresh.Name = "button_Refresh";
            this.button_Refresh.Size = new System.Drawing.Size(75, 23);
            this.button_Refresh.TabIndex = 8;
            this.button_Refresh.Text = "Atnaujinti";
            this.button_Refresh.UseVisualStyleBackColor = true;
            this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click_2);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 12);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 20);
            this.label18.TabIndex = 4;
            this.label18.Text = "Paieška:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label16.Location = new System.Drawing.Point(651, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 15);
            this.label16.TabIndex = 3;
            this.label16.Text = "Filtras:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Visi",
            "Esantis darbe",
            "Neatėję "});
            this.comboBox1.Location = new System.Drawing.Point(694, 13);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.watch_ComboBox);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.monthCalendar1);
            this.groupBox7.Controls.Add(this.dataGridView3);
            this.groupBox7.Location = new System.Drawing.Point(822, 38);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(484, 466);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Neatpažinti asmenys";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(319, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Rodomo registro diena";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(322, 31);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 3;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.watch_DateChanged);
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToOrderColumns = true;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.neat_ID,
            this.neat_nuotrauka,
            this.Laikas,
            this.Pral_Punktas});
            this.dataGridView3.Location = new System.Drawing.Point(7, 17);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(303, 447);
            this.dataGridView3.TabIndex = 0;
            // 
            // neat_ID
            // 
            this.neat_ID.DataPropertyName = "ID";
            this.neat_ID.HeaderText = "ID";
            this.neat_ID.Name = "neat_ID";
            this.neat_ID.ReadOnly = true;
            this.neat_ID.Visible = false;
            this.neat_ID.Width = 40;
            // 
            // neat_nuotrauka
            // 
            this.neat_nuotrauka.DataPropertyName = "BPKS_Nuotrauka";
            this.neat_nuotrauka.HeaderText = "Nuotrauka";
            this.neat_nuotrauka.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.neat_nuotrauka.Name = "neat_nuotrauka";
            this.neat_nuotrauka.ReadOnly = true;
            this.neat_nuotrauka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.neat_nuotrauka.Width = 60;
            // 
            // Laikas
            // 
            this.Laikas.DataPropertyName = "Laikas";
            this.Laikas.HeaderText = "Laikas";
            this.Laikas.Name = "Laikas";
            this.Laikas.ReadOnly = true;
            // 
            // Pral_Punktas
            // 
            this.Pral_Punktas.DataPropertyName = "Pral_Punktas";
            this.Pral_Punktas.HeaderText = "Pral. Punktas";
            this.Pral_Punktas.Name = "Pral_Punktas";
            this.Pral_Punktas.ReadOnly = true;
            this.Pral_Punktas.Width = 80;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dataGridView1);
            this.groupBox6.Location = new System.Drawing.Point(4, 38);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(812, 469);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Einamasis registras";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ein_Asm_ID,
            this.ein_Vardas,
            this.ein_Pavarde,
            this.ein_Pareigos,
            this.ein_Darbo_Vieta,
            this.ein_Laikas,
            this.ein_Pral_Punktas,
            this.ein_Tel_Nr,
            this.ein_El_pastas,
            this.ein_Darb_Nuotrauka,
            this.ein_Nuotrauka});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 16);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(806, 450);
            this.dataGridView1.TabIndex = 0;
            // 
            // ein_Asm_ID
            // 
            this.ein_Asm_ID.DataPropertyName = "Asm_ID";
            this.ein_Asm_ID.HeaderText = "Asmens ID";
            this.ein_Asm_ID.Name = "ein_Asm_ID";
            this.ein_Asm_ID.ReadOnly = true;
            this.ein_Asm_ID.Width = 85;
            // 
            // ein_Vardas
            // 
            this.ein_Vardas.DataPropertyName = "Vardas";
            this.ein_Vardas.HeaderText = "Vardas";
            this.ein_Vardas.Name = "ein_Vardas";
            this.ein_Vardas.ReadOnly = true;
            // 
            // ein_Pavarde
            // 
            this.ein_Pavarde.DataPropertyName = "Pavarde";
            this.ein_Pavarde.HeaderText = "Pavardė";
            this.ein_Pavarde.Name = "ein_Pavarde";
            this.ein_Pavarde.ReadOnly = true;
            // 
            // ein_Pareigos
            // 
            this.ein_Pareigos.DataPropertyName = "Pareigos";
            this.ein_Pareigos.HeaderText = "Pareigos";
            this.ein_Pareigos.Name = "ein_Pareigos";
            this.ein_Pareigos.ReadOnly = true;
            // 
            // ein_Darbo_Vieta
            // 
            this.ein_Darbo_Vieta.DataPropertyName = "Darbo_Vieta";
            this.ein_Darbo_Vieta.HeaderText = "Darbo vieta";
            this.ein_Darbo_Vieta.Name = "ein_Darbo_Vieta";
            this.ein_Darbo_Vieta.ReadOnly = true;
            this.ein_Darbo_Vieta.Width = 80;
            // 
            // ein_Laikas
            // 
            this.ein_Laikas.DataPropertyName = "Laikas";
            this.ein_Laikas.HeaderText = "Laikas";
            this.ein_Laikas.Name = "ein_Laikas";
            this.ein_Laikas.ReadOnly = true;
            // 
            // ein_Pral_Punktas
            // 
            this.ein_Pral_Punktas.DataPropertyName = "Pral_Punktas";
            this.ein_Pral_Punktas.HeaderText = "Pral. Punktas";
            this.ein_Pral_Punktas.Name = "ein_Pral_Punktas";
            this.ein_Pral_Punktas.ReadOnly = true;
            // 
            // ein_Tel_Nr
            // 
            this.ein_Tel_Nr.DataPropertyName = "Tel_Nr";
            this.ein_Tel_Nr.HeaderText = "Telefono nr.";
            this.ein_Tel_Nr.Name = "ein_Tel_Nr";
            this.ein_Tel_Nr.ReadOnly = true;
            // 
            // ein_El_pastas
            // 
            this.ein_El_pastas.DataPropertyName = "El_pastas";
            this.ein_El_pastas.HeaderText = "El. Paštas";
            this.ein_El_pastas.Name = "ein_El_pastas";
            this.ein_El_pastas.ReadOnly = true;
            this.ein_El_pastas.Visible = false;
            // 
            // ein_Darb_Nuotrauka
            // 
            this.ein_Darb_Nuotrauka.DataPropertyName = "Darb_Nuotrauka";
            this.ein_Darb_Nuotrauka.HeaderText = "Darb. Nuotrauka";
            this.ein_Darb_Nuotrauka.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.ein_Darb_Nuotrauka.Name = "ein_Darb_Nuotrauka";
            this.ein_Darb_Nuotrauka.ReadOnly = true;
            this.ein_Darb_Nuotrauka.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ein_Darb_Nuotrauka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ein_Darb_Nuotrauka.Width = 60;
            // 
            // ein_Nuotrauka
            // 
            this.ein_Nuotrauka.DataPropertyName = "BPKS_Nuotrauka";
            this.ein_Nuotrauka.HeaderText = "BPKS nuotrauka";
            this.ein_Nuotrauka.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.ein_Nuotrauka.Name = "ein_Nuotrauka";
            this.ein_Nuotrauka.ReadOnly = true;
            this.ein_Nuotrauka.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ein_Nuotrauka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ein_Nuotrauka.Width = 60;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button_PDF);
            this.panel4.Controls.Add(this.button_Excel);
            this.panel4.Controls.Add(this.groupBox9);
            this.panel4.Controls.Add(this.textBox10);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.groupBox8);
            this.panel4.Location = new System.Drawing.Point(11, 87);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1320, 513);
            this.panel4.TabIndex = 0;
            // 
            // button_PDF
            // 
            this.button_PDF.Location = new System.Drawing.Point(907, 25);
            this.button_PDF.Name = "button_PDF";
            this.button_PDF.Size = new System.Drawing.Size(141, 42);
            this.button_PDF.TabIndex = 12;
            this.button_PDF.Text = "Eksportuoti į PDF";
            this.button_PDF.UseVisualStyleBackColor = true;
            this.button_PDF.Click += new System.EventHandler(this.button_toPDF);
            // 
            // button_Excel
            // 
            this.button_Excel.Location = new System.Drawing.Point(1102, 25);
            this.button_Excel.Name = "button_Excel";
            this.button_Excel.Size = new System.Drawing.Size(141, 42);
            this.button_Excel.TabIndex = 11;
            this.button_Excel.Text = "Eksportuoti į Excel";
            this.button_Excel.UseVisualStyleBackColor = true;
            this.button_Excel.Click += new System.EventHandler(this.button_toExcel);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.chart1);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.label_SelPerson);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.textBox12);
            this.groupBox9.Controls.Add(this.textBox11);
            this.groupBox9.Controls.Add(this.comboBox2);
            this.groupBox9.Location = new System.Drawing.Point(846, 93);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(471, 411);
            this.groupBox9.TabIndex = 10;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Duomenų vizualizavimas";
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(6, 116);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(454, 290);
            this.chart1.TabIndex = 12;
            this.chart1.Text = "chart1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label25.Location = new System.Drawing.Point(190, 68);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 13);
            this.label25.TabIndex = 11;
            this.label25.Text = "(yyyy-mm-dd)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label24.Location = new System.Drawing.Point(54, 68);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(67, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "(yyyy-mm-dd)";
            // 
            // label_SelPerson
            // 
            this.label_SelPerson.AutoSize = true;
            this.label_SelPerson.Location = new System.Drawing.Point(128, 33);
            this.label_SelPerson.Name = "label_SelPerson";
            this.label_SelPerson.Size = new System.Drawing.Size(0, 13);
            this.label_SelPerson.TabIndex = 9;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label23.Location = new System.Drawing.Point(8, 33);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(113, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "Pasirinktas asmuo:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(304, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "Grafiko tipas:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label21.Location = new System.Drawing.Point(151, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(20, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "iki";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label20.Location = new System.Drawing.Point(9, 91);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Nuo";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(177, 84);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 3;
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox11.Location = new System.Drawing.Point(45, 85);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 2;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Darbo laikas per mėnesį"});
            this.comboBox2.Location = new System.Drawing.Point(307, 84);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(158, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.reg_DataVisualization);
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox10.Location = new System.Drawing.Point(93, 9);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(307, 26);
            this.textBox10.TabIndex = 9;
            this.textBox10.TextChanged += new System.EventHandler(this.reg_Search);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label19.Location = new System.Drawing.Point(10, 14);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 20);
            this.label19.TabIndex = 8;
            this.label19.Text = "Paieška:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dataGridView4);
            this.groupBox8.Location = new System.Drawing.Point(3, 41);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(837, 466);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Registras";
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.AllowUserToOrderColumns = true;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.reg_Asm_ID,
            this.reg_Vardas,
            this.reg_Pavarde,
            this.reg_Pareigos,
            this.reg_Darbo_Vieta,
            this.reg_Atejo,
            this.reg_Isejo,
            this.reg_Darbo_Laikas,
            this.reg_Pral_Punktas,
            this.reg_Tel_Nr,
            this.reg_El_pastas,
            this.reg_Nuotrauka,
            this.reg_BPKS_Nuotrauka1,
            this.reg_BPKS_Nuotrauka2,
            this.reg_Data});
            this.dataGridView4.Location = new System.Drawing.Point(11, 14);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.Size = new System.Drawing.Size(818, 449);
            this.dataGridView4.TabIndex = 0;
            this.dataGridView4.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.regSelPerson_CellClick);
            // 
            // reg_Asm_ID
            // 
            this.reg_Asm_ID.DataPropertyName = "Asm_ID";
            this.reg_Asm_ID.HeaderText = "Asmens ID";
            this.reg_Asm_ID.Name = "reg_Asm_ID";
            this.reg_Asm_ID.ReadOnly = true;
            this.reg_Asm_ID.Width = 85;
            // 
            // reg_Vardas
            // 
            this.reg_Vardas.DataPropertyName = "Vardas";
            this.reg_Vardas.HeaderText = "Vardas";
            this.reg_Vardas.Name = "reg_Vardas";
            this.reg_Vardas.ReadOnly = true;
            // 
            // reg_Pavarde
            // 
            this.reg_Pavarde.DataPropertyName = "Pavarde";
            this.reg_Pavarde.HeaderText = "Pavardė";
            this.reg_Pavarde.Name = "reg_Pavarde";
            this.reg_Pavarde.ReadOnly = true;
            // 
            // reg_Pareigos
            // 
            this.reg_Pareigos.DataPropertyName = "Pareigos";
            this.reg_Pareigos.HeaderText = "Pareigos";
            this.reg_Pareigos.Name = "reg_Pareigos";
            this.reg_Pareigos.ReadOnly = true;
            // 
            // reg_Darbo_Vieta
            // 
            this.reg_Darbo_Vieta.DataPropertyName = "Darbo_Vieta";
            this.reg_Darbo_Vieta.HeaderText = "Darbo vieta";
            this.reg_Darbo_Vieta.Name = "reg_Darbo_Vieta";
            this.reg_Darbo_Vieta.ReadOnly = true;
            this.reg_Darbo_Vieta.Width = 90;
            // 
            // reg_Atejo
            // 
            this.reg_Atejo.DataPropertyName = "Atejo";
            this.reg_Atejo.HeaderText = "Atėjo";
            this.reg_Atejo.Name = "reg_Atejo";
            this.reg_Atejo.ReadOnly = true;
            // 
            // reg_Isejo
            // 
            this.reg_Isejo.DataPropertyName = "Isejo";
            this.reg_Isejo.HeaderText = "Išėjo";
            this.reg_Isejo.Name = "reg_Isejo";
            this.reg_Isejo.ReadOnly = true;
            // 
            // reg_Darbo_Laikas
            // 
            this.reg_Darbo_Laikas.DataPropertyName = "Darbo_Laikas";
            this.reg_Darbo_Laikas.HeaderText = "Darbo laikas";
            this.reg_Darbo_Laikas.Name = "reg_Darbo_Laikas";
            this.reg_Darbo_Laikas.ReadOnly = true;
            // 
            // reg_Pral_Punktas
            // 
            this.reg_Pral_Punktas.DataPropertyName = "Pral_Punktas";
            this.reg_Pral_Punktas.HeaderText = "Pral. Punktas";
            this.reg_Pral_Punktas.Name = "reg_Pral_Punktas";
            this.reg_Pral_Punktas.ReadOnly = true;
            this.reg_Pral_Punktas.Width = 85;
            // 
            // reg_Tel_Nr
            // 
            this.reg_Tel_Nr.DataPropertyName = "Tel_Nr";
            this.reg_Tel_Nr.HeaderText = "Tel. Numeris";
            this.reg_Tel_Nr.Name = "reg_Tel_Nr";
            this.reg_Tel_Nr.ReadOnly = true;
            // 
            // reg_El_pastas
            // 
            this.reg_El_pastas.DataPropertyName = "El_pastas";
            this.reg_El_pastas.HeaderText = "El. Paštas";
            this.reg_El_pastas.Name = "reg_El_pastas";
            this.reg_El_pastas.ReadOnly = true;
            // 
            // reg_Nuotrauka
            // 
            this.reg_Nuotrauka.DataPropertyName = "Darb_Nuotrauka";
            this.reg_Nuotrauka.HeaderText = "Nuotrauka";
            this.reg_Nuotrauka.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.reg_Nuotrauka.Name = "reg_Nuotrauka";
            this.reg_Nuotrauka.ReadOnly = true;
            this.reg_Nuotrauka.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.reg_Nuotrauka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.reg_Nuotrauka.Width = 60;
            // 
            // reg_BPKS_Nuotrauka1
            // 
            this.reg_BPKS_Nuotrauka1.DataPropertyName = "BPKS_Nuotrauka1";
            this.reg_BPKS_Nuotrauka1.HeaderText = "BPKS nuotrauka";
            this.reg_BPKS_Nuotrauka1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.reg_BPKS_Nuotrauka1.Name = "reg_BPKS_Nuotrauka1";
            this.reg_BPKS_Nuotrauka1.ReadOnly = true;
            this.reg_BPKS_Nuotrauka1.Width = 60;
            // 
            // reg_BPKS_Nuotrauka2
            // 
            this.reg_BPKS_Nuotrauka2.DataPropertyName = "BPKS_Nuotrauka2";
            this.reg_BPKS_Nuotrauka2.HeaderText = "BPKS nuotrauka";
            this.reg_BPKS_Nuotrauka2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.reg_BPKS_Nuotrauka2.Name = "reg_BPKS_Nuotrauka2";
            this.reg_BPKS_Nuotrauka2.ReadOnly = true;
            this.reg_BPKS_Nuotrauka2.Width = 60;
            // 
            // reg_Data
            // 
            this.reg_Data.DataPropertyName = "Data";
            this.reg_Data.HeaderText = "Data";
            this.reg_Data.Name = "reg_Data";
            this.reg_Data.ReadOnly = true;
            this.reg_Data.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_DeletePerson);
            this.panel2.Controls.Add(this.button_Save_Updated_Data);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.clearSearchField);
            this.panel2.Controls.Add(this.dataGridView2);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.textBox8);
            this.panel2.Location = new System.Drawing.Point(12, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1298, 497);
            this.panel2.TabIndex = 2;
            // 
            // button_DeletePerson
            // 
            this.button_DeletePerson.Location = new System.Drawing.Point(1156, 424);
            this.button_DeletePerson.Name = "button_DeletePerson";
            this.button_DeletePerson.Size = new System.Drawing.Size(112, 40);
            this.button_DeletePerson.TabIndex = 20;
            this.button_DeletePerson.Text = "Ištrinti asmenį";
            this.button_DeletePerson.UseVisualStyleBackColor = true;
            this.button_DeletePerson.Click += new System.EventHandler(this.button_DeletePerson_Click);
            // 
            // button_Save_Updated_Data
            // 
            this.button_Save_Updated_Data.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_Save_Updated_Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.button_Save_Updated_Data.Location = new System.Drawing.Point(989, 40);
            this.button_Save_Updated_Data.Name = "button_Save_Updated_Data";
            this.button_Save_Updated_Data.Size = new System.Drawing.Size(253, 40);
            this.button_Save_Updated_Data.TabIndex = 19;
            this.button_Save_Updated_Data.Text = "Išsaugoti atnaujintus duomenis";
            this.button_Save_Updated_Data.UseVisualStyleBackColor = false;
            this.button_Save_Updated_Data.Click += new System.EventHandler(this.button_Save_Updated_Data_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox_Browse_File);
            this.groupBox5.Controls.Add(this.Button_Browse_File);
            this.groupBox5.Controls.Add(this.pictureBox3);
            this.groupBox5.Location = new System.Drawing.Point(729, 121);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(239, 320);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Asmens nuotrauka";
            // 
            // textBox_Browse_File
            // 
            this.textBox_Browse_File.Location = new System.Drawing.Point(16, 285);
            this.textBox_Browse_File.Name = "textBox_Browse_File";
            this.textBox_Browse_File.Size = new System.Drawing.Size(126, 20);
            this.textBox_Browse_File.TabIndex = 21;
            // 
            // Button_Browse_File
            // 
            this.Button_Browse_File.Location = new System.Drawing.Point(148, 282);
            this.Button_Browse_File.Name = "Button_Browse_File";
            this.Button_Browse_File.Size = new System.Drawing.Size(65, 25);
            this.Button_Browse_File.TabIndex = 20;
            this.Button_Browse_File.Text = "Naršyti..";
            this.Button_Browse_File.UseVisualStyleBackColor = true;
            this.Button_Browse_File.Click += new System.EventHandler(this.Button_Browse_File_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pictureBox3.Location = new System.Drawing.Point(16, 19);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(200, 250);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.text_Edit_ID);
            this.groupBox4.Controls.Add(this.text_Edit_Workplace);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.text_Edit_Position);
            this.groupBox4.Controls.Add(this.text_Edit_Email);
            this.groupBox4.Controls.Add(this.text_Edit_Phone);
            this.groupBox4.Controls.Add(this.text_Edit_Surname);
            this.groupBox4.Controls.Add(this.text_Edit_Name);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.groupBox4.Location = new System.Drawing.Point(989, 121);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(299, 278);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Asmens informacija";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label8.Location = new System.Drawing.Point(6, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "Asmens ID";
            // 
            // text_Edit_ID
            // 
            this.text_Edit_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.text_Edit_ID.Location = new System.Drawing.Point(101, 36);
            this.text_Edit_ID.Name = "text_Edit_ID";
            this.text_Edit_ID.ReadOnly = true;
            this.text_Edit_ID.Size = new System.Drawing.Size(97, 21);
            this.text_Edit_ID.TabIndex = 18;
            // 
            // text_Edit_Workplace
            // 
            this.text_Edit_Workplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.text_Edit_Workplace.Location = new System.Drawing.Point(101, 229);
            this.text_Edit_Workplace.Name = "text_Edit_Workplace";
            this.text_Edit_Workplace.Size = new System.Drawing.Size(192, 21);
            this.text_Edit_Workplace.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label10.Location = new System.Drawing.Point(11, 229);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 16);
            this.label10.TabIndex = 16;
            this.label10.Text = "Darbo vieta";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label11.Location = new System.Drawing.Point(9, 198);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 16);
            this.label11.TabIndex = 15;
            this.label11.Text = "Pareigos";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label12.Location = new System.Drawing.Point(9, 164);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 16);
            this.label12.TabIndex = 14;
            this.label12.Text = "El. Paštas";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label13.Location = new System.Drawing.Point(9, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 16);
            this.label13.TabIndex = 9;
            this.label13.Text = "Telefono nr.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label14.Location = new System.Drawing.Point(9, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 16);
            this.label14.TabIndex = 8;
            this.label14.Text = "Pavardė";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label15.Location = new System.Drawing.Point(9, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 16);
            this.label15.TabIndex = 7;
            this.label15.Text = "Vardas";
            // 
            // text_Edit_Position
            // 
            this.text_Edit_Position.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.text_Edit_Position.Location = new System.Drawing.Point(101, 198);
            this.text_Edit_Position.Name = "text_Edit_Position";
            this.text_Edit_Position.Size = new System.Drawing.Size(192, 21);
            this.text_Edit_Position.TabIndex = 12;
            // 
            // text_Edit_Email
            // 
            this.text_Edit_Email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.text_Edit_Email.Location = new System.Drawing.Point(101, 164);
            this.text_Edit_Email.Name = "text_Edit_Email";
            this.text_Edit_Email.Size = new System.Drawing.Size(192, 21);
            this.text_Edit_Email.TabIndex = 13;
            // 
            // text_Edit_Phone
            // 
            this.text_Edit_Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.text_Edit_Phone.Location = new System.Drawing.Point(101, 137);
            this.text_Edit_Phone.Name = "text_Edit_Phone";
            this.text_Edit_Phone.Size = new System.Drawing.Size(192, 21);
            this.text_Edit_Phone.TabIndex = 3;
            // 
            // text_Edit_Surname
            // 
            this.text_Edit_Surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.text_Edit_Surname.Location = new System.Drawing.Point(101, 102);
            this.text_Edit_Surname.Name = "text_Edit_Surname";
            this.text_Edit_Surname.Size = new System.Drawing.Size(192, 21);
            this.text_Edit_Surname.TabIndex = 2;
            // 
            // text_Edit_Name
            // 
            this.text_Edit_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.text_Edit_Name.Location = new System.Drawing.Point(101, 69);
            this.text_Edit_Name.Name = "text_Edit_Name";
            this.text_Edit_Name.Size = new System.Drawing.Size(192, 21);
            this.text_Edit_Name.TabIndex = 1;
            // 
            // clearSearchField
            // 
            this.clearSearchField.Location = new System.Drawing.Point(395, 17);
            this.clearSearchField.Name = "clearSearchField";
            this.clearSearchField.Size = new System.Drawing.Size(63, 23);
            this.clearSearchField.TabIndex = 9;
            this.clearSearchField.Text = "Išvalyti";
            this.clearSearchField.UseVisualStyleBackColor = true;
            this.clearSearchField.Click += new System.EventHandler(this.clearSearchField_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Asm_ID,
            this.Vardas,
            this.Pavarde,
            this.Tel_Nr,
            this.El_pastas,
            this.Pareigos,
            this.Darbo_Vieta,
            this.Darb_Nuotrauka});
            this.dataGridView2.Location = new System.Drawing.Point(17, 67);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(694, 397);
            this.dataGridView2.TabIndex = 8;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid2_CellClick);
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid2_CellClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            this.ID.Width = 30;
            // 
            // Asm_ID
            // 
            this.Asm_ID.DataPropertyName = "Asm_ID";
            this.Asm_ID.HeaderText = "Asm. ID";
            this.Asm_ID.Name = "Asm_ID";
            this.Asm_ID.ReadOnly = true;
            this.Asm_ID.Width = 70;
            // 
            // Vardas
            // 
            this.Vardas.DataPropertyName = "Vardas";
            this.Vardas.HeaderText = "Vardas";
            this.Vardas.Name = "Vardas";
            this.Vardas.ReadOnly = true;
            // 
            // Pavarde
            // 
            this.Pavarde.DataPropertyName = "Pavarde";
            this.Pavarde.HeaderText = "Pavardė";
            this.Pavarde.Name = "Pavarde";
            this.Pavarde.ReadOnly = true;
            // 
            // Tel_Nr
            // 
            this.Tel_Nr.DataPropertyName = "Tel_Nr";
            this.Tel_Nr.HeaderText = "Tel. Nr.";
            this.Tel_Nr.Name = "Tel_Nr";
            this.Tel_Nr.ReadOnly = true;
            this.Tel_Nr.Width = 80;
            // 
            // El_pastas
            // 
            this.El_pastas.DataPropertyName = "El_pastas";
            this.El_pastas.HeaderText = "El. Paštas";
            this.El_pastas.Name = "El_pastas";
            this.El_pastas.ReadOnly = true;
            // 
            // Pareigos
            // 
            this.Pareigos.DataPropertyName = "Pareigos";
            this.Pareigos.HeaderText = "Pareigos";
            this.Pareigos.Name = "Pareigos";
            this.Pareigos.ReadOnly = true;
            // 
            // Darbo_Vieta
            // 
            this.Darbo_Vieta.DataPropertyName = "Darbo_Vieta";
            this.Darbo_Vieta.HeaderText = "Darbo vieta";
            this.Darbo_Vieta.Name = "Darbo_Vieta";
            this.Darbo_Vieta.ReadOnly = true;
            // 
            // Darb_Nuotrauka
            // 
            this.Darb_Nuotrauka.DataPropertyName = "Darb_Nuotrauka";
            this.Darb_Nuotrauka.HeaderText = "Darb_Nuotrauka";
            this.Darb_Nuotrauka.Name = "Darb_Nuotrauka";
            this.Darb_Nuotrauka.ReadOnly = true;
            this.Darb_Nuotrauka.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label9.Location = new System.Drawing.Point(3, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 20);
            this.label9.TabIndex = 7;
            this.label9.Text = "Paieška:";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox8.Location = new System.Drawing.Point(81, 17);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(307, 26);
            this.textBox8.TabIndex = 6;
            this.textBox8.TextChanged += new System.EventHandler(this.data_ManageSearch);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1337, 606);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Biometrinė praėjimo kontrolės sistema - administravimo programa";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton addPerson;
        private System.Windows.Forms.ToolStripButton dataManagement;
        private System.Windows.Forms.ToolStripButton watch;
        private System.Windows.Forms.ToolStripButton register;
        private System.Windows.Forms.ToolStripButton about;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button toPersonAddForm;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button next_button;
        private System.Windows.Forms.Button prev_button;
        private System.Windows.Forms.TextBox textBox_image_path;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Asm_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pavarde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tel_Nr;
        private System.Windows.Forms.DataGridViewTextBoxColumn El_pastas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pareigos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Darbo_Vieta;
        private System.Windows.Forms.DataGridViewImageColumn Darb_Nuotrauka;
        private System.Windows.Forms.Button clearSearchField;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox text_Edit_Workplace;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox text_Edit_Position;
        private System.Windows.Forms.TextBox text_Edit_Email;
        private System.Windows.Forms.TextBox text_Edit_Phone;
        private System.Windows.Forms.TextBox text_Edit_Surname;
        private System.Windows.Forms.TextBox text_Edit_Name;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox text_Edit_ID;
        private System.Windows.Forms.Button button_Save_Updated_Data;
        private System.Windows.Forms.TextBox textBox_Browse_File;
        private System.Windows.Forms.Button Button_Browse_File;
        private System.Windows.Forms.Button button_DeletePerson;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Asm_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Pavarde;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Pareigos;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Darbo_Vieta;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Laikas;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Pral_Punktas;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_Tel_Nr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ein_El_pastas;
        private System.Windows.Forms.DataGridViewImageColumn ein_Darb_Nuotrauka;
        private System.Windows.Forms.DataGridViewImageColumn ein_Nuotrauka;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label_SelPerson;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Asm_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Pavarde;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Pareigos;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Darbo_Vieta;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Atejo;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Isejo;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Darbo_Laikas;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Pral_Punktas;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Tel_Nr;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_El_pastas;
        private System.Windows.Forms.DataGridViewImageColumn reg_Nuotrauka;
        private System.Windows.Forms.DataGridViewImageColumn reg_BPKS_Nuotrauka1;
        private System.Windows.Forms.DataGridViewImageColumn reg_BPKS_Nuotrauka2;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_Data;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button_Excel;
        private System.Windows.Forms.Button button_PDF;
        private System.Windows.Forms.Button button_Refresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn neat_ID;
        private System.Windows.Forms.DataGridViewImageColumn neat_nuotrauka;
        private System.Windows.Forms.DataGridViewTextBoxColumn Laikas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pral_Punktas;
    }
}

