﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Image = System.Drawing.Image;

//

namespace BPKS_administratoriaus_programa
{
    public partial class Form1 : Form
    {
        #region Variables
        string reg_SelPersonID = "";
        int pos_pic_box = 0;
        List<Bitmap> bitMapList = new List<Bitmap>();
        //List<Bitmap> BMPList = new List<Bitmap>();
        int count = 0;
        //This is my connection string i have assigned the database file address path
        string MyConnection = "datasource=127.0.0.1;port=3306;username=root;password=Modestas";
        #endregion
        public Form1()
        {
            InitializeComponent();
            SetMyButtonProperties();
            //Disable form resizing for users 
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            addPerson_Click(null, null);
            next_button.Enabled = false;
            prev_button.Enabled = false;
        }

        public void return_From_Add_Person()
        {
            MySqlConnection myConn = new MySqlConnection(MyConnection);
            myConn.Open();
            try
            {
                MySqlCommand command = new MySqlCommand(" select ID,BPKS_Nuotrauka from  bpks.asm_nuotraukos where Asm_ID ='" + this.textBox1.Text + "' order by ID;", myConn);
                //maxID = command.ExecuteScalar().ToString();
            }
            finally
            {
                myConn.Close();
            }
            try
            {
                //This is my insert query in which i am taking input from the user through windows forms
                string Query = " select BPKS_Nuotrauka from  bpks.asm_nuotraukos where Asm_ID ='" + this.textBox1.Text + "' order by ID;";
                //This is  MySqlConnection here i have created the object and pass my connection string.
                MySqlConnection MyConn = new MySqlConnection(MyConnection);
                //This is command class which will handle the query and connection object.
                MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyCommand.ExecuteReader();     //Here our query will be executed and data saved into the database.
                count = 0;
                while (MyReader.Read())
                {
                    count = count + 1;
                    string picdata = MyReader.GetString("BPKS_Nuotrauka");
                    byte[] picData = MyReader["BPKS_Nuotrauka"] as byte[] ?? null;
                    //Convert bit array to bitmap
                    ImageConverter pic = new ImageConverter();
                    Image img = (Image)pic.ConvertFrom(MyReader["BPKS_Nuotrauka"]);
                    Bitmap bmp = new Bitmap(img);
                    bitMapList.Add(bmp);
                }
                MyConn.Close();
                pictureBox2.Image = bitMapList[0];
                toPersonAddForm.Enabled = false;
                next_button.Enabled = true;
                prev_button.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addPerson_Click(object sender, EventArgs e)
        {
            person_ID_generation();
            //Add_Person_Photo TF = new Add_Person_Photo();
            //TF.Show();
            //Show panel 1
            panel1.Visible = true;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
        }
        private void dataShow_Watch_Un(String query)
        {
            try
            {
                //String query = String.Format("SELECT * FROM bpks.asm_neatpazinti");
                MySqlConnection conn = new MySqlConnection(MyConnection);
                conn.Open();
                MySqlCommand command = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                //Display data to grid
                DataTable data = new DataTable();
                adapter.Fill(data);
                dataGridView3.DataSource = data;
                conn.Close();
            }
            //If Exception show error message
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dataShow_Watch_Reg(String query)
        {
            try
            {
                //String query = String.Format("SELECT a.Asm_ID, a.Laikas, a.Pral_Punktas, a.Nuotrauka, b.Vardas, b.Pavarde, b.Tel_Nr, b.El_pastas, b.Pareigos, b.Darbo_Vieta, b.Darb_Nuotrauka FROM bpks.asm_registras_einamasis a LEFT JOIN bpks.asm_dosje b ON a.Asm_ID = b.Asm_ID");
                MySqlConnection conn = new MySqlConnection(MyConnection);
                conn.Open();
                MySqlCommand command = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                //Display data to grid
                DataTable data = new DataTable();
                adapter.Fill(data);
                dataGridView1.DataSource = data;
                conn.Close();
            }
            //If Exception show error message
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dataShow(String query)
        {
            try
            {
                MySqlConnection conn = new MySqlConnection(MyConnection);
                conn.Open();
                MySqlCommand command = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                //Display data to grid
                DataTable data = new DataTable();
                adapter.Fill(data);
                dataGridView2.DataSource = data;
                conn.Close();
            }
            //If Exception show error message
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dataShow_Reg(String query)
        {
            try
            {
                MySqlConnection conn = new MySqlConnection(MyConnection);
                conn.Open();
                MySqlCommand command = new MySqlCommand(query, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                //Display data to grid
                DataTable data = new DataTable();
                adapter.Fill(data);
                dataGridView4.DataSource = data;
                conn.Close();
            }
            //If Exception show error message
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataManagement_Click(object sender, EventArgs e)
        {
            clear_DataManagement();
            //Data grid show
            dataShow(String.Format("SELECT * FROM bpks.asm_dosje"));
            panel1.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
            panel4.Visible = false;
        }
        private void watch_Click(object sender, EventArgs e)
        {
            dataShow_Watch_Un(String.Format("SELECT * FROM bpks.asm_neatpazinti ORDER BY Laikas"));
            comboBox1.SelectedIndex = 0;
            dataShow_Watch_Reg(String.Format("SELECT a.Asm_ID, a.Vardas, a.Pavarde, a.Tel_Nr, a.El_pastas, a.Pareigos, a.Darbo_Vieta, a.Darb_Nuotrauka, b.Laikas, b.Pral_Punktas, b.BPKS_Nuotrauka FROM bpks.asm_dosje a LEFT JOIN bpks.asm_registras_einamasis b ON a.Asm_ID = b.Asm_ID ORDER BY a.Asm_ID"));
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = true;
            panel4.Visible = false;
        }
        private void register_Click(object sender, EventArgs e)
        {
            int year = DateTime.Now.Year;
            DateTime firstDay = new DateTime(year , 1, 1);

            DateTime lastDay = new DateTime(year , 12, 31);
            textBox11.Text = firstDay.ToString().Substring(0, 10);
            textBox12.Text = lastDay.ToString().Substring(0, 10); ;
            reg_SelPersonID = "";
            dataShow_Reg(String.Format("SELECT a.Atejo, a.Isejo, a.Pral_Punktas, a.BPKS_Nuotrauka1, a.BPKS_Nuotrauka2, a.Darbo_Laikas, a.Data, b.Asm_ID,b.Vardas, b.Pavarde, b.Tel_Nr, b.El_pastas, b.Pareigos, b.Darbo_Vieta, b.Darb_Nuotrauka FROM bpks.asm_registras a LEFT JOIN bpks.asm_dosje b ON a.Asm_ID = b.Asm_ID ORDER BY a.ID"));
            label_SelPerson.Text = "";
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = true;
        }
        private void toPersonAddForm_Click(object sender, EventArgs e)
        {
            //Check or person ID field is not null
            if (textBox1.Text != String.Empty)
            {
                Add_Person_Photo TF = new Add_Person_Photo(textBox1.Text);
                TF.Show();
            }
            else MessageBox.Show("Pirmiausiai turite įvesti asmens ID");
        }
        //Set button properties
        private void SetMyButtonProperties()
        {
            //Assign an image to the button.
            button_Save_Updated_Data.Image = Image.FromFile(Application.StartupPath + "/Resources/1431295269_stock_save.png");
            button2.Image = Image.FromFile(Application.StartupPath + "/Resources/1431295269_stock_save.png");
            button3.Image = Image.FromFile(Application.StartupPath + "/Resources/1430958018_clear-left.png");
            toPersonAddForm.Image = Image.FromFile(Application.StartupPath + "/Resources/User-Interface-Face-Recognition-Scan-icon.png");
            button_DeletePerson.Image = Image.FromFile(Application.StartupPath + "/Resources/1431358795_f-cross_256-32.png");
            button_Excel.Image = Image.FromFile(Application.StartupPath + "/Resources/1431520711_647702-excel-32.png");
            button_PDF.Image = Image.FromFile(Application.StartupPath + "/Resources/1431520724_647704-pdf-32.png");
            button_Refresh.Image = Image.FromFile(Application.StartupPath + "/Resources/1432380966_refresh.png");
            //Align the image and text on the button.
            button_Refresh.ImageAlign = ContentAlignment.MiddleRight;
            button_Refresh.TextAlign = ContentAlignment.MiddleLeft;
            button_DeletePerson.ImageAlign = ContentAlignment.MiddleRight;
            button_DeletePerson.TextAlign = ContentAlignment.MiddleLeft;
            button_Save_Updated_Data.ImageAlign = ContentAlignment.MiddleRight;
            button_Save_Updated_Data.TextAlign = ContentAlignment.MiddleLeft;
            button2.ImageAlign = ContentAlignment.MiddleRight;
            button2.TextAlign = ContentAlignment.MiddleLeft;
            button_Excel.ImageAlign = ContentAlignment.MiddleRight;
            button_Excel.TextAlign = ContentAlignment.MiddleLeft;
            button_PDF.ImageAlign = ContentAlignment.MiddleRight;
            button_PDF.TextAlign = ContentAlignment.MiddleLeft;
            toPersonAddForm.TextAlign = ContentAlignment.MiddleLeft;
            toPersonAddForm.ImageAlign = ContentAlignment.MiddleRight;
            button3.ImageAlign = ContentAlignment.MiddleRight;
            button3.TextAlign = ContentAlignment.MiddleLeft;
            //Give the button a flat appearance.
            // button2.FlatStyle = FlatStyle.Flat;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Clear form fields
            clear_Form_fields();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Picture upload 
            OpenFileDialog OpenFd = new OpenFileDialog();
            //Filter
            OpenFd.Filter = "Images only. |*.jpg; *.jpeg; *.png; *.gif;";
            //DialogResult dr = OpenFd.ShowDialog();
            if (OpenFd.ShowDialog() == DialogResult.OK)
            {
                //Display uploaded picture in picture box
                string picPath = OpenFd.FileName.ToString();
                textBox_image_path.Text = picPath;
                pictureBox1.ImageLocation = picPath;
                //  pictureBox1.Image = Image.FromFile(OpenFd.FileName);
            }

        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] imageBt = null;
                // Initializes a new instance of the FileStream class
                FileStream fstream = new FileStream(this.textBox_image_path.Text, FileMode.Open, FileAccess.Read);
                // Reads primitive data types as binary values
                BinaryReader br = new BinaryReader(fstream);
                imageBt = br.ReadBytes((int)fstream.Length);

                //This is my insert query in which i am taking input from the user through windows forms
                string Query = "insert into bpks.asm_dosje(Asm_ID,Vardas,Pavarde,Tel_Nr,El_pastas,Pareigos,Darbo_Vieta,Darb_Nuotrauka) values('" + this.textBox1.Text + "','" + this.textBox2.Text + "','" + this.textBox3.Text + "','" + this.textBox4.Text + "','" + this.textBox5.Text + "','" + this.textBox6.Text + "','" + this.textBox7.Text + "',@IMG);";
                //This is  MySqlConnection here i have created the object and pass my connection string.
                MySqlConnection MyConn = new MySqlConnection(MyConnection);
                //This is command class which will handle the query and connection object.
                MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyCommand.Parameters.Add(new MySqlParameter("IMG", imageBt));
                MyReader = MyCommand.ExecuteReader();     //Here our query will be executed and data saved into the database.
                MessageBox.Show("Asmuo pridėtas sėkmingai");
                clear_Form_fields();
                next_button.Enabled = false;
                prev_button.Enabled = false;
                while (MyReader.Read())
                {
                }
                MyConn.Close();
                toPersonAddForm.Enabled = true;
                // Clear face picture list
                bitMapList.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void clear_Form_fields()
        {
            //Clear text box field
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox_image_path.Clear();
            //Clear picureBox
            pictureBox1.Image = null;
            pictureBox2.Image = null;
        }
        private void clear_DataManagement()
        { 
             //Clear field, pictureBox and button disable
             text_Edit_ID.Clear();
             text_Edit_Name.Clear();
             text_Edit_Surname.Clear();
             text_Edit_Phone.Clear();
             text_Edit_Email.Clear();
             text_Edit_Position.Clear();
             text_Edit_Workplace.Clear();
             textBox_Browse_File.Clear();
             pictureBox3.Image = null;
             Button_Browse_File.Enabled = false;
             button_DeletePerson.Enabled = false;
             button_Save_Updated_Data.Enabled = false;
        }
        public void person_ID_generation()
        {
            string maxID =null;
            MySqlConnection conn = new MySqlConnection(MyConnection);
            conn.Open();
            try
            {
                MySqlCommand command = new MySqlCommand("select max(ID) from bpks.asm_dosje", conn);
                maxID = command.ExecuteScalar().ToString();
            }
            finally
            {
                if (maxID == String.Empty) maxID = "1";
                conn.Close();
                //Person ID lenght
                int lenght = 5;
                //ID value
                string mask = "ASM";
                for (int i = 0; i < lenght - maxID.Length; i++) mask = (mask + "0");
                textBox1.Text = mask+maxID;
            }
        }
        private void button7_Click(object sender, EventArgs e)
        {
            person_ID_generation();
        }

        //BPKS face image of each person display control
        private void prev_button_Click(object sender, EventArgs e)
        {
            pos_pic_box--;
            if (pos_pic_box < 0) pos_pic_box = count-1;
            pictureBox2.Image = bitMapList[pos_pic_box];
        }
        //BPKS face image of each person display control
        private void next_button_Click(object sender, EventArgs e)
        {
            pos_pic_box++;
            if (pos_pic_box > count-1) pos_pic_box = 0;
            pictureBox2.Image = bitMapList[pos_pic_box];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            return_From_Add_Person();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //pictureBox2.Image = bitMapList[0];
             //return_From_Add_Person();
            var sk = bitMapList.Count;
            MessageBox.Show(sk.ToString());
        }

        //Cell click event
        private void dataGrid2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >=0)
            {
                DataGridViewRow row = this.dataGridView2.Rows[e.RowIndex];
                //Display person foto
                //MessageBox.Show(row.Cells["pavardeDataGridViewTextBoxColumn"].Value.ToString());
                textBox_Browse_File.Clear();
                Button_Browse_File.Enabled = true;
                button_DeletePerson.Enabled = true;
                button_Save_Updated_Data.Enabled = true;
                MemoryStream ms = new MemoryStream((byte[])row.Cells["Darb_Nuotrauka"].Value);
                pictureBox3.Image = Image.FromStream(ms);
                text_Edit_ID.Text = row.Cells["Asm_ID"].Value.ToString();
                text_Edit_Name.Text = row.Cells["Vardas"].Value.ToString();
                text_Edit_Surname.Text = row.Cells["Pavarde"].Value.ToString();
                text_Edit_Phone.Text = row.Cells["Tel_Nr"].Value.ToString();
                text_Edit_Email.Text = row.Cells["El_pastas"].Value.ToString();
                text_Edit_Position.Text = row.Cells["Pareigos"].Value.ToString();
                text_Edit_Workplace.Text = row.Cells["Darbo_Vieta"].Value.ToString();
            }
        }

        // Search
        private void data_ManageSearch(object sender, EventArgs e)
        {
            string value = textBox8.Text;
            dataShow(String.Format("SELECT * FROM bpks.asm_dosje WHERE Vardas LIKE '%" + value + "%' OR Pavarde LIKE '%" + value + "%' OR Asm_ID LIKE '%" + value + "%'"));

        }
        private void clearSearchField_Click(object sender, EventArgs e)
        {
            textBox8.Clear();
        }

        private void button_Save_Updated_Data_Click(object sender, EventArgs e)
        {
            try
            {
                //This is my update query in which i am taking input from the user through windows forms and update the record.
                string Query;
                // insert into bpks.asm_dosje(Asm_ID,Vardas,Pavarde,Tel_Nr,El_pastas,Pareigos,Darbo_Vieta,Darb_Nuotrauka) values('" + this.textBox1.Text + "','" + this.textBox2.Text + "','" + this.textBox3.Text + "','" + this.textBox4.Text + "','" + this.textBox5.Text + "','" + this.textBox6.Text + "','" + this.textBox7.Text + "',@IMG);";
                if (textBox_Browse_File.Text == String.Empty) Query = "update bpks.asm_dosje set Vardas='" + this.text_Edit_Name.Text + "',Pavarde='" + this.text_Edit_Surname.Text + "',Tel_Nr='" + this.text_Edit_Phone.Text + "',El_pastas='" + this.text_Edit_Email.Text + "',Pareigos='" + this.text_Edit_Position.Text + "',Darbo_Vieta='" + this.text_Edit_Workplace.Text + "' where Asm_ID='" + this.text_Edit_ID.Text + "';";
                else Query = "update bpks.asm_dosje set Vardas='" + this.text_Edit_Name.Text + "',Pavarde='" + this.text_Edit_Surname.Text + "',Tel_Nr='" + this.text_Edit_Phone.Text + "',El_pastas='" + this.text_Edit_Email.Text + "',Pareigos='" + this.text_Edit_Position.Text + "',Darbo_Vieta='" + this.text_Edit_Workplace.Text + "', Darb_nuotrauka = @IMG where Asm_ID='" + this.text_Edit_ID.Text + "';";
                //This is  MySqlConnection here i have created the object and pass my connection string.
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                if (textBox_Browse_File.Text != String.Empty)
                {
                    byte[] imageBt = null;
                    // Initializes a new instance of the FileStream class
                    FileStream fstream = new FileStream(this.textBox_Browse_File.Text, FileMode.Open, FileAccess.Read);
                    // Reads primitive data types as binary values
                    BinaryReader br = new BinaryReader(fstream);
                    imageBt = br.ReadBytes((int)fstream.Length);
                    MyCommand2.Parameters.Add(new MySqlParameter("IMG", imageBt));
                } 
                MyReader2 = MyCommand2.ExecuteReader();
                MessageBox.Show("Duomenis atnaujinti");
                while (MyReader2.Read())
                {
                }
                MyConn2.Close();//Connection closed 
            }
            catch (Exception ex)
           { 
                MessageBox.Show(ex.Message);
           }
           clear_DataManagement();
           dataShow(String.Format("SELECT * FROM bpks.asm_dosje"));
       }

        private void Button_Browse_File_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFd = new OpenFileDialog();
            //Filter
            OpenFd.Filter = "Images only. |*.jpg; *.jpeg; *.png; *.gif;";
            if (OpenFd.ShowDialog() == DialogResult.OK)
            {
                //Display uploaded picture in picture box
                string picPath = OpenFd.FileName.ToString();
                textBox_Browse_File.Text = picPath;
                pictureBox3.ImageLocation = picPath;
            }
        }
        private void delete_data(String Query)
        {
            try
            {
                 MySqlConnection MyConn2 = new MySqlConnection(MyConnection);
                 MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                 MySqlDataReader MyReader2;
                 MyConn2.Open();
                 MyReader2 = MyCommand2.ExecuteReader();
                 while (MyReader2.Read())
                 {
                 }
                 MyConn2.Close();
            }
            catch (Exception ex)
            {
                 MessageBox.Show(ex.Message);
            }
        }
        private void button_DeletePerson_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Ar tikrai norite ištrinti šį asmenį?", "Dėmesio!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                String qdata = String.Format("delete from bpks.asm_dosje where Asm_ID='" + this.text_Edit_ID.Text + "';");
                String qpic = String.Format("delete from bpks.asm_nuotraukos where Asm_ID='" + this.text_Edit_ID.Text + "';");
                String qreg1 = String.Format("delete from bpks.asm_registras where Asm_ID='" + this.text_Edit_ID.Text + "';");
                String qreg2 = String.Format("delete from bpks.asm_registras_einamasis where Asm_ID='" + this.text_Edit_ID.Text + "';");
                delete_data(qdata);
                delete_data(qpic);
                delete_data(qreg1);
                delete_data(qreg2);
                clear_DataManagement();
                dataShow(String.Format("SELECT * FROM bpks.asm_dosje"));
                MessageBox.Show("Asmuo ištrintas");
            } 
        }
        private void watch_ComboBox(object sender, EventArgs e)
        {
            textBox9.Clear();
            int selectedIndex = comboBox1.SelectedIndex;
            if (selectedIndex == 0) dataShow_Watch_Reg(String.Format("SELECT a.Asm_ID, a.Vardas, a.Pavarde, a.Tel_Nr, a.El_pastas, a.Pareigos, a.Darbo_Vieta, a.Darb_Nuotrauka, b.Laikas, b.Pral_Punktas, b.BPKS_Nuotrauka FROM bpks.asm_dosje a LEFT JOIN bpks.asm_registras_einamasis b ON a.Asm_ID = b.Asm_ID;"));
            if (selectedIndex == 1) dataShow_Watch_Reg(String.Format("SELECT a.Asm_ID, a.Vardas, a.Pavarde, a.Tel_Nr, a.El_pastas, a.Pareigos, a.Darbo_Vieta, a.Darb_Nuotrauka, b.Laikas, b.Pral_Punktas, b.BPKS_Nuotrauka FROM bpks.asm_dosje a LEFT JOIN bpks.asm_registras_einamasis b ON a.Asm_ID = b.Asm_ID WHERE b.Laikas IS NOT NULL;"));
            if (selectedIndex == 2) dataShow_Watch_Reg(String.Format("SELECT a.Asm_ID, a.Vardas, a.Pavarde, a.Tel_Nr, a.El_pastas, a.Pareigos, a.Darbo_Vieta, a.Darb_Nuotrauka, b.Laikas, b.Pral_Punktas, b.BPKS_Nuotrauka FROM bpks.asm_dosje a LEFT JOIN bpks.asm_registras_einamasis b ON a.Asm_ID = b.Asm_ID  WHERE b.Laikas IS NULL;"));
        }

        private void watch_DateChanged(object sender, DateRangeEventArgs e)
        {
            string selDate = monthCalendar1.SelectionRange.Start.ToShortDateString();
            dataShow_Watch_Un(String.Format("SELECT * FROM bpks.asm_neatpazinti WHERE Laikas LIKE '%" + selDate + "%' ORDER BY LAIKAS;"));
        }
        private void watch_UnCellClick(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            string value = textBox9.Text;
            dataShow_Watch_Reg(String.Format("SELECT a.Asm_ID, a.Vardas, a.Pavarde, a.Tel_Nr, a.El_pastas, a.Pareigos, a.Darbo_Vieta, a.Darb_Nuotrauka, b.Laikas, b.Pral_Punktas, b.BPKS_Nuotrauka FROM bpks.asm_dosje a LEFT JOIN bpks.asm_registras_einamasis b ON a.Asm_ID = b.Asm_ID WHERE a.Vardas LIKE '%" + value + "%' OR a.Pavarde LIKE '%" + value + "%' OR a.Asm_ID LIKE '%" + value + "%' ORDER BY a.Asm_ID"));
        }
        private void reg_Search(object sender, EventArgs e)
        {
            string value = textBox10.Text;
            dataShow_Reg(String.Format("SELECT a.Atejo, a.Isejo, a.Pral_Punktas, a.BPKS_Nuotrauka1, a.BPKS_Nuotrauka2, a.Darbo_Laikas, a.Data, b.Asm_ID, b.Vardas, b.Pavarde, b.Tel_Nr, b.El_pastas, b.Pareigos, b.Darbo_Vieta, b.Darb_Nuotrauka FROM bpks.asm_registras a LEFT JOIN bpks.asm_dosje b ON a.Asm_ID = b.Asm_ID WHERE b.Vardas LIKE '%" + value + "%' OR b.Pavarde LIKE '%" + value + "%' OR b.Asm_ID LIKE '%" + value + "%'  ORDER BY a.ID"));
        }
        private void regSelPerson_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView4.Rows[e.RowIndex];
                reg_SelPersonID = row.Cells["reg_Asm_ID"].Value.ToString();
                String person = ("" + row.Cells["reg_Vardas"].Value.ToString() + " " + row.Cells["reg_Pavarde"].Value.ToString() + " (" + row.Cells["reg_Asm_ID"].Value.ToString() + ")"); 
                label_SelPerson.Text = person;
            }
        }
        private void reg_DataVisualization(object sender, EventArgs e)
        {
            string nuo = textBox11.Text;
            string iki = textBox12.Text;
            int selectedIndex = comboBox1.SelectedIndex;
            List<string> list1 = new List<string>();
            List<double> list2 = new List<double>();
            list1.Clear();
            list2.Clear();
            if ((reg_SelPersonID != String.Empty) && (nuo != String.Empty) && (iki != String.Empty))
            {
                //Data arrays.
                try
                {
                   // string Query = "SELECT Data, Darbo_Laikas FROM bpks.asm_registras WHERE Asm_ID = '"+reg_SelPersonID+"' ORDER BY ID";
                    string Query = "SELECT Data, Darbo_Laikas FROM bpks.asm_registras WHERE Asm_ID = '" + reg_SelPersonID + "' AND (Data BETWEEN '"+nuo+"' AND '"+iki+"') ORDER BY ID";
                    MySqlConnection MyConn = new MySqlConnection(MyConnection);
                    //This is command class which will handle the query and connection object.
                    MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyCommand.ExecuteReader();
                    count = 0;
                    while (MyReader.Read())
                    {
                         string data = MyReader["Data"].ToString();
                         //Substring data
                         data = data.Substring(0, 19);
                         //list1.Add(MyReader["Data"].ToString());
                         list1.Add(data);
                         string val = MyReader["Darbo_Laikas"].ToString();
                         double seconds = TimeSpan.Parse(val).TotalSeconds;
                         list2.Add(seconds);
                         count++;
                    }
                    MyConn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("labas" + ex.Message);
                }
                if (count > 0)
                {
                    //Clear chart
                    chart1.Series.Clear();
                    chart1.Titles.Clear();
                    //chart1.ChartAreas.Clear();
                    //chart1.Legends.Clear();
                    int max = (Convert.ToInt32(list2.Max() / 3600)) + 1;
                    if (max >= 1) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 1, 3601 * 1, "1h");
                    if (max >= 2) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 2, 3601 * 2, "2h");
                    if (max >= 3) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 3, 3601 * 3, "3h");
                    if (max >= 4) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 4, 3601 * 4, "4h");
                    if (max >= 5) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 5, 3601 * 5, "5h");
                    if (max >= 6) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 6, 3601 * 6, "6h");
                    if (max >= 7) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 7, 3601 * 7, "7h");
                    if (max >= 8) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 8, 3601 * 8, "8h");
                    if (max >= 9) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 9, 3601 * 9, "9h");
                    if (max >= 10) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 10, 3601 * 10, "10h");
                    if (max >= 11) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 11, 3601 * 11, "11h");
                    if (max >= 12) chart1.ChartAreas[0].AxisY.CustomLabels.Add(3599 * 12, 3601 * 12, "12h");
                    chart1.ChartAreas[0].AxisY.Interval = 3600;
                    chart1.ChartAreas[0].AxisX.Interval = 100;
                    chart1.ChartAreas[0].AxisX.IntervalOffset = 10;
                   // chart1.ChartAreas[0].AxisX.Maximum = 3;
                    chart1.ChartAreas[0].AxisX.Minimum = 0;
                    this.chart1.Titles.Add("Asmens darbo laikas per laikotarpį");
                    //Add series.
                    for (int i = 0; i < count; i++)
                    {
                        //Add series.
                        Series series = this.chart1.Series.Add(list1[i]);
                        //Add point.
                        series.Points.Add(list2[i]);
                    }
                }
                else
                {
                    chart1.Series.Clear();
                    chart1.Titles.Clear();
                    MessageBox.Show("Pasirinktame intervale duomenų nerasta");
                }

            }
               else 
                MessageBox.Show("Nenurodyta data arba asmuo");
        }
        private void ToExcel(DataGridView dGV, string filename)
        {
            string stOutput = "";
            // Export titles:
            string sHeaders = "";

            for (int j = 0; j < dGV.Columns.Count; j++)
                sHeaders = sHeaders.ToString() + Convert.ToString(dGV.Columns[j].HeaderText) + "\t";
            stOutput += sHeaders + "\r\n";
            // Export data.
            for (int i = 0; i < dGV.RowCount - 1; i++)
            {
                string stLine = "";
                for (int j = 0; j < dGV.Rows[i].Cells.Count; j++)
                    stLine = stLine.ToString() + Convert.ToString(dGV.Rows[i].Cells[j].Value) + "\t";
                //if (stLine ) BPKS nuotrauka
                stOutput += stLine + "\r\n";
            }
            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] output = utf16.GetBytes(stOutput);
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(output, 0, output.Length); //write the encoded file
            bw.Flush();
            bw.Close();
            fs.Close();
        }
        private void button_toExcel(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "BPKS_Data_Export.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                ToExcel(dataGridView4, sfd.FileName); 
            }
        }

        private void button_toPDF(object sender, EventArgs e)
        {
            //Creating iTextSharp Table from the DataTable data
            PdfPTable pdfTable = new PdfPTable(dataGridView4.ColumnCount);
            pdfTable.DefaultCell.Padding = 3;
            pdfTable.WidthPercentage = 100;
            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable.DefaultCell.BorderWidth = 1;

            //Adding Header row
            foreach (DataGridViewColumn column in dataGridView4.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                pdfTable.AddCell(cell);
            }

            //Adding DataRow
            foreach (DataGridViewRow row in dataGridView4.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdfTable.AddCell(cell.Value.ToString());
                }
            }
            //Exporting to PDF
            SaveFileDialog svg = new SaveFileDialog();
            svg.Filter = "Excel Documents (*.pdf)|*.pdf";
            svg.FileName = "BPKS_Data_Export.pdf";
            svg.ShowDialog();

            using (FileStream stream = new FileStream(svg.FileName, FileMode.Create))
            {
                Document pdfDoc = new Document(PageSize.A1, 10f, 10f, 10f, 0f);
                PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                pdfDoc.Add(pdfTable);
                pdfDoc.Close();
                stream.Close();
            }
        }

        private void about_Click(object sender, EventArgs e)
        {
            about_Form TF = new about_Form();
            TF.Show();
        }

        private void button_Refresh_Click_2(object sender, EventArgs e)
        {
            dataShow_Watch_Un(String.Format("SELECT * FROM bpks.asm_neatpazinti ORDER BY Laikas"));
            dataShow_Watch_Reg(String.Format("SELECT a.Asm_ID, a.Vardas, a.Pavarde, a.Tel_Nr, a.El_pastas, a.Pareigos, a.Darbo_Vieta, a.Darb_Nuotrauka, b.Laikas, b.Pral_Punktas, b.BPKS_Nuotrauka FROM bpks.asm_dosje a LEFT JOIN bpks.asm_registras_einamasis b ON a.Asm_ID = b.Asm_ID ORDER BY a.Asm_ID"));
        }

    }

}
