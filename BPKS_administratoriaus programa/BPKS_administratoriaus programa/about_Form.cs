﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPKS_administratoriaus_programa
{
    public partial class about_Form : Form
    {
        public about_Form()
        {
            InitializeComponent();
        }

        private void about_Form_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            pictureAbout.Image = Image.FromFile(Application.StartupPath + "/Resources/400px-Elektronika.jpg");
        }
    }
}
