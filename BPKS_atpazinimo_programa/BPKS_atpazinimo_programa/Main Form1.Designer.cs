﻿namespace BPKS_atpazinimo_programa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.message_bar = new System.Windows.Forms.Label();
            this.button_start_stop = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.time = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_Time = new System.Windows.Forms.Label();
            this.label_Position = new System.Windows.Forms.Label();
            this.label_Surname = new System.Windows.Forms.Label();
            this.label_Name = new System.Windows.Forms.Label();
            this.label_AsmID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox_Foto = new System.Windows.Forms.PictureBox();
            this.label_Status = new System.Windows.Forms.Label();
            this.image_PIC = new System.Windows.Forms.PictureBox();
            this.pictureBox_Status = new System.Windows.Forms.PictureBox();
            this.label_fiksavimo_laikas = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox_detected = new System.Windows.Forms.PictureBox();
            this.pictureBox_matched = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label_last_update = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Foto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.image_PIC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_detected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_matched)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // message_bar
            // 
            this.message_bar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.message_bar.AutoSize = true;
            this.message_bar.Location = new System.Drawing.Point(12, 413);
            this.message_bar.Name = "message_bar";
            this.message_bar.Size = new System.Drawing.Size(53, 13);
            this.message_bar.TabIndex = 1;
            this.message_bar.Text = "Message:";
            // 
            // button_start_stop
            // 
            this.button_start_stop.Location = new System.Drawing.Point(946, 12);
            this.button_start_stop.Name = "button_start_stop";
            this.button_start_stop.Size = new System.Drawing.Size(122, 29);
            this.button_start_stop.TabIndex = 5;
            this.button_start_stop.Text = "Paleisti/Stabdyti";
            this.button_start_stop.UseVisualStyleBackColor = false;
            this.button_start_stop.Click += new System.EventHandler(this.button_StartStop_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.time.ForeColor = System.Drawing.Color.ForestGreen;
            this.time.Location = new System.Drawing.Point(106, 12);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(56, 25);
            this.time.TabIndex = 8;
            this.time.Text = "time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(12, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Laikas:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(520, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 37);
            this.label3.TabIndex = 0;
            this.label3.Text = "Būsena:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_Time);
            this.groupBox1.Controls.Add(this.label_Position);
            this.groupBox1.Controls.Add(this.label_Surname);
            this.groupBox1.Controls.Add(this.label_Name);
            this.groupBox1.Controls.Add(this.label_AsmID);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.groupBox1.Location = new System.Drawing.Point(527, 129);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 225);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Asmens informacija";
            // 
            // label_Time
            // 
            this.label_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_Time.ForeColor = System.Drawing.Color.Black;
            this.label_Time.Location = new System.Drawing.Point(156, 175);
            this.label_Time.Name = "label_Time";
            this.label_Time.Size = new System.Drawing.Size(164, 19);
            this.label_Time.TabIndex = 0;
            this.label_Time.Text = "-";
            // 
            // label_Position
            // 
            this.label_Position.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_Position.ForeColor = System.Drawing.Color.Black;
            this.label_Position.Location = new System.Drawing.Point(91, 136);
            this.label_Position.Name = "label_Position";
            this.label_Position.Size = new System.Drawing.Size(229, 29);
            this.label_Position.TabIndex = 0;
            this.label_Position.Text = "-";
            // 
            // label_Surname
            // 
            this.label_Surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_Surname.ForeColor = System.Drawing.Color.Black;
            this.label_Surname.Location = new System.Drawing.Point(91, 102);
            this.label_Surname.Name = "label_Surname";
            this.label_Surname.Size = new System.Drawing.Size(229, 18);
            this.label_Surname.TabIndex = 0;
            this.label_Surname.Text = "-";
            // 
            // label_Name
            // 
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_Name.ForeColor = System.Drawing.Color.Black;
            this.label_Name.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label_Name.Location = new System.Drawing.Point(83, 66);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(236, 18);
            this.label_Name.TabIndex = 0;
            this.label_Name.Text = "-";
            // 
            // label_AsmID
            // 
            this.label_AsmID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_AsmID.ForeColor = System.Drawing.Color.Black;
            this.label_AsmID.Location = new System.Drawing.Point(114, 30);
            this.label_AsmID.Name = "label_AsmID";
            this.label_AsmID.Size = new System.Drawing.Size(197, 18);
            this.label_AsmID.TabIndex = 0;
            this.label_AsmID.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(6, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 20);
            this.label9.TabIndex = 5;
            this.label9.Text = "Fiksavimo laikas:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(6, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "Pareigos:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(6, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Pavardė:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(6, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Vardas:";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label.Location = new System.Drawing.Point(6, 30);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(102, 20);
            this.label.TabIndex = 0;
            this.label.Text = "Asmens ID:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pictureBox_Foto);
            this.groupBox2.Location = new System.Drawing.Point(859, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 225);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Asmens nuotrauka";
            // 
            // pictureBox_Foto
            // 
            this.pictureBox_Foto.BackColor = System.Drawing.Color.LightGray;
            this.pictureBox_Foto.Location = new System.Drawing.Point(6, 20);
            this.pictureBox_Foto.Name = "pictureBox_Foto";
            this.pictureBox_Foto.Size = new System.Drawing.Size(200, 200);
            this.pictureBox_Foto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Foto.TabIndex = 6;
            this.pictureBox_Foto.TabStop = false;
            // 
            // label_Status
            // 
            this.label_Status.AutoSize = true;
            this.label_Status.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold);
            this.label_Status.ForeColor = System.Drawing.Color.Black;
            this.label_Status.Location = new System.Drawing.Point(639, 46);
            this.label_Status.Name = "label_Status";
            this.label_Status.Size = new System.Drawing.Size(26, 37);
            this.label_Status.TabIndex = 11;
            this.label_Status.Text = "-";
            // 
            // image_PIC
            // 
            this.image_PIC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.image_PIC.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.image_PIC.Location = new System.Drawing.Point(12, 46);
            this.image_PIC.Name = "image_PIC";
            this.image_PIC.Size = new System.Drawing.Size(480, 360);
            this.image_PIC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.image_PIC.TabIndex = 2;
            this.image_PIC.TabStop = false;
            // 
            // pictureBox_Status
            // 
            this.pictureBox_Status.Location = new System.Drawing.Point(946, 48);
            this.pictureBox_Status.Name = "pictureBox_Status";
            this.pictureBox_Status.Size = new System.Drawing.Size(100, 75);
            this.pictureBox_Status.TabIndex = 12;
            this.pictureBox_Status.TabStop = false;
            // 
            // label_fiksavimo_laikas
            // 
            this.label_fiksavimo_laikas.AutoSize = true;
            this.label_fiksavimo_laikas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_fiksavimo_laikas.Location = new System.Drawing.Point(12, 184);
            this.label_fiksavimo_laikas.Name = "label_fiksavimo_laikas";
            this.label_fiksavimo_laikas.Size = new System.Drawing.Size(124, 16);
            this.label_fiksavimo_laikas.TabIndex = 13;
            this.label_fiksavimo_laikas.Text = "00:00:00.0000000";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 60000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // pictureBox_detected
            // 
            this.pictureBox_detected.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox_detected.Location = new System.Drawing.Point(15, 28);
            this.pictureBox_detected.Name = "pictureBox_detected";
            this.pictureBox_detected.Size = new System.Drawing.Size(140, 140);
            this.pictureBox_detected.TabIndex = 14;
            this.pictureBox_detected.TabStop = false;
            // 
            // pictureBox_matched
            // 
            this.pictureBox_matched.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox_matched.Location = new System.Drawing.Point(15, 218);
            this.pictureBox_matched.Name = "pictureBox_matched";
            this.pictureBox_matched.Size = new System.Drawing.Size(140, 140);
            this.pictureBox_matched.TabIndex = 15;
            this.pictureBox_matched.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label_last_update);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.pictureBox_detected);
            this.groupBox3.Controls.Add(this.label_fiksavimo_laikas);
            this.groupBox3.Controls.Add(this.pictureBox_matched);
            this.groupBox3.Location = new System.Drawing.Point(1076, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(167, 406);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            // 
            // label_last_update
            // 
            this.label_last_update.AutoSize = true;
            this.label_last_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label_last_update.Location = new System.Drawing.Point(15, 389);
            this.label_last_update.Name = "label_last_update";
            this.label_last_update.Size = new System.Drawing.Size(142, 16);
            this.label_last_update.TabIndex = 21;
            this.label_last_update.Text = "0000-00-00 00:00:00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 376);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Duomenys atnaujinti:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Identifikuotas veidas";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Aptiktas veidas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Identifikavimo laikas:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 414);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pictureBox_Status);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label_Status);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.time);
            this.Controls.Add(this.button_start_stop);
            this.Controls.Add(this.image_PIC);
            this.Controls.Add(this.message_bar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "BPKS atpažinimo programa";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Foto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.image_PIC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_detected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_matched)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label message_bar;
        private System.Windows.Forms.PictureBox image_PIC;
        private System.Windows.Forms.Button button_start_stop;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label_Time;
        private System.Windows.Forms.Label label_Position;
        private System.Windows.Forms.Label label_Surname;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.Label label_AsmID;
        private System.Windows.Forms.Label label_Status;
        private System.Windows.Forms.PictureBox pictureBox_Foto;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox_Status;
        private System.Windows.Forms.Label label_fiksavimo_laikas;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox pictureBox_detected;
        private System.Windows.Forms.PictureBox pictureBox_matched;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_last_update;
        private System.Windows.Forms.Label label12;
    }
}