﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Microsoft.Win32.SafeHandles;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Xml;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Diagnostics;
using MySql.Data.MySqlClient;
// Emgu Library
using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

namespace BPKS_atpazinimo_programa
{
    public partial class Form1 : Form
    {
        #region variables
        //string MyConnection = "datasource=127.0.0.1;port=3306;username=root;password=Modestas";
        string MyConnection = "datasource=127.0.0.1;port=3306;username=root;password=Modestas";
        bool check_DB_conn;
        int buttonClick = 0;
        long start_time = 0, elapsed_time = 0;
        string count;
        //Current image aquired from webcam for display
        Image<Bgr, Byte> currentFrame;
        //Store the result image and prepared faces
        Image<Gray, byte> result = null;
        //Grayscale current image aquired from webcam for processing
        Image<Gray, byte> gray_frame = null; 
        Capture grabber;
        // Haar Cascade Classifier path
        public CascadeClassifier Face = new CascadeClassifier(Application.StartupPath + "/HaarCascades/haarcascade_frontalface_default.xml");

        //emgu class Recognize constructor
        Recognize person_Recog = new Recognize();

        #endregion
        public Form1()
        {
            InitializeComponent();
            //Load person data from database
            if (person_Recog.IsTrained)
            {
                message_bar.Text = "Asmenų veido duomenis įkelti";
            }
            else
            {
                message_bar.Text = "Asmenų veidų duomenų bazėje nerasta";
            }
           //Start capture
        }
        //Check MySQL connection state
        public bool checkDBConnection()

        {
            bool result = false;
            MySqlConnection connection = new MySqlConnection(MyConnection);
            try
            {
                connection.Open();
                result = true;
                connection.Close();
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show("Prisijungimo prie duomenų bazės klaida: "+ex.Message);
            }
            return result;
        }

        public void retrain()
        {
            person_Recog = new Recognize();
            if (person_Recog.IsTrained)
            {
                message_bar.Text = "Asmenų veido duomenis įkelti";
            }
            else
            {
                message_bar.Text = "Asmenų veidų duomenų bazėje nerasta";
            }
        }
        //Start capture
        public void initialise_capture()
        {
            grabber = new Capture();
            grabber.QueryFrame();
            //Initialize the FrameGraber event
            Application.Idle += new EventHandler(FrameGrabber);
            pictureBox_Status.Image = Image.FromFile(Application.StartupPath + "/Resources/status.gif");
        }
        //Stop the FrameGraber event
        private void stop_capture()
        {
            Application.Idle -= new EventHandler(FrameGrabber);
            if (grabber != null)
            {
                grabber.Dispose();
            }
            pictureBox_Status.Image = Image.FromFile(Application.StartupPath + "/Resources/status_stop.PNG");
        }
        //Process Frame
        void FrameGrabber(object sender, EventArgs e)
        {
            //Get the current frame form capture device
            currentFrame = grabber.QueryFrame().Resize(320, 240, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
            //Calc elapsed time
            elapsed_time = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - start_time; 
            if (currentFrame != null)
            {
                if (elapsed_time >= 4000) clearFields();
                //Convert to Grayscale
                gray_frame = currentFrame.Convert<Gray, Byte>();
                //Face Detector
                //     Rectangle[] facesDetected = Face.DetectMultiScale(gray_frame, 1.2, 10, new Size(140, 140), Size.Empty);
                Rectangle[] facesDetected = Face.DetectMultiScale(gray_frame, 1.2, 10, new Size(140, 140), Size.Empty);
                //Action for each face detected
                for (int i = 0; i < facesDetected.Length; i++)// (Rectangle face_found in facesDetected)
                {
                    //Remove a majoriy of the background noise
                    facesDetected[i].X += (int)(facesDetected[i].Height * 0.15);
                    facesDetected[i].Y += (int)(facesDetected[i].Width * 0.22);
                    facesDetected[i].Height -= (int)(facesDetected[i].Height * 0.3);
                    facesDetected[i].Width -= (int)(facesDetected[i].Width * 0.35);
                    //Save result
                    result = currentFrame.Copy(facesDetected[i]).Convert<Gray, byte>().Resize(140, 140, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                    //That improves the contrast in an image, in order to stretch out the intensity range.
                    result._EqualizeHist();
                    //Draw the face detected in the 0th (gray) channel with blue color
                    currentFrame.Draw(facesDetected[i], new Bgr(Color.LimeGreen), 2);
                    //Check or recognizer is trained and time is elapsed
                    if ((person_Recog.IsTrained) && (elapsed_time >= 4000))
                    {
                        var watch = Stopwatch.StartNew();
                        //Get recognition result
                        //string ID = person_Recog.Recognise(result);
                        string img_ID = person_Recog.Recognise(result);
                        string ID = "";
                        //
                        //Display detected face to imagebox
                        pictureBox_detected.Image = result.ToBitmap();
                        //showDetails(ID);
                        //log to bpks.asm_neatpazinti
                        if (img_ID == "unrecognized")
                        {
                            ID = img_ID;
                            showDetails(ID);
                            pictureBox_matched.Image = null;
                            recogLog(String.Format("insert into bpks.asm_neatpazinti(Pral_Punktas,Laikas,BPKS_Nuotrauka) values('Nr. 1','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',@IMG);"), result);
                        }
                        //log to bpks.asm_registras_einamasis;
                        else
                        {
                            //
                            //Return person id and display mached picture to the imagebox
                            try
                            {
                                string Query = "SELECT Asm_ID,BPKS_Nuotrauka FROM bpks.asm_nuotraukos WHERE ID = '" + img_ID + "' ";
                                MySqlConnection MyConn = new MySqlConnection(MyConnection);
                                MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                                MySqlDataReader MyReader;
                                MyConn.Open();
                                MyReader = MyCommand.ExecuteReader();
                                while (MyReader.Read())
                                {
                                    ID = MyReader.GetString("Asm_ID");
                                    string picdata = MyReader.GetString("BPKS_Nuotrauka");
                                    byte[] picData = MyReader["BPKS_Nuotrauka"] as byte[] ?? null;
                                    ImageConverter pic = new ImageConverter();
                                    Image img = (Image)pic.ConvertFrom(MyReader["BPKS_Nuotrauka"]);
                                    Bitmap bmp = new Bitmap(img);
                                    pictureBox_matched.Image = bmp;
                                }
                                MyConn.Close();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                            showDetails(ID);
                            recogLog(String.Format("insert into bpks.asm_registras_einamasis(Asm_ID,Laikas,Pral_Punktas,BPKS_Nuotrauka) values('" + ID + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','Nr. 1',@IMG);"), result);
                        }
                        ifExit(ID);
                        //Display elapsed time
                        watch.Stop();
                        label_fiksavimo_laikas.Text = watch.Elapsed.ToString();
                        //
                        //string name = person_Recog.Recognise(result);
                        //int match_value = (int)person_Recog.Get_Distance;
                        //label_Distance.Text = match_value.ToString();
                        //detected = true;
                        //Get time in milliseconds
                        start_time = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
                    }
                }
                //Show procesed and recognized face
                image_PIC.Image = currentFrame.ToBitmap();
            }
        }

        //Show details about the person
        void showDetails(string ID)
        {
            if (ID == "unrecognized")
            {
                string soundName = "unrecognized.wav";
                label_Status.ForeColor = System.Drawing.Color.Red;
                label_Status.Text = "Asmuo neatpažintas";
                label_Time.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                playSound(soundName);
            }
            else
            {
                string soundName = "recognized.wav";
                label_Status.ForeColor = System.Drawing.Color.ForestGreen;
                label_Status.Text = "Asmuo atpažintas";
                label_AsmID.Text = ID;
                label_Time.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                try
                {
                    string Query = "SELECT Vardas,Pavarde,Pareigos,Darb_Nuotrauka FROM bpks.asm_dosje WHERE Asm_ID = '" + ID + "' ";
                    MySqlConnection MyConn = new MySqlConnection(MyConnection);
                    MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyCommand.ExecuteReader(); 
                    while (MyReader.Read())
                    {
                        label_Name.Text = MyReader.GetString("Vardas");
                        label_Surname.Text = MyReader.GetString("Pavarde");
                        label_Position.Text = MyReader.GetString("Pareigos");
                        string picdata = MyReader.GetString("Darb_Nuotrauka");
                        byte[] picData = MyReader["Darb_Nuotrauka"] as byte[] ?? null;
                        ImageConverter pic = new ImageConverter();
                        Image img = (Image)pic.ConvertFrom(MyReader["Darb_Nuotrauka"]);
                        Bitmap bmp = new Bitmap(img);
                        pictureBox_Foto.Image = bmp;
                    }
                    MyConn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                playSound(soundName);
            }
        }
        void clearFields()
        {
            label_Status.ForeColor = System.Drawing.Color.DarkCyan;
            label_Status.Text = "Ieškoma veido";
            label_AsmID.Text = "";
            label_Name.Text = "";
            label_Surname.Text = "";
            label_Position.Text = "";
            label_Time.Text = "";
            pictureBox_Foto.Image = null;
        }
        void recogLog(string Query, Image<Gray, byte> detectedFace)    
        {
            try
                {
                    //Byte array for converted images
                    byte[][] saveImage = new byte[1][];
                    //Using MemoryStream
                    using (MemoryStream ms = new MemoryStream())
                    {
                        //Convert Image to byte[]
                        detectedFace.Bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                        byte[] imageBytes = ms.ToArray();

                        saveImage[0] = imageBytes;
                    }
                    //This is my insert query in which i am taking input from the user through windows forms
                    //string Query = "insert into bpks.asm_neatpazinti(Pral_Punktas,Laikas,BPKS_Nuotrauka) values('Nr. 1','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +"',@IMG);";
                    //This is  MySqlConnection here i have created the object and pass my connection string.
                    MySqlConnection MyConn = new MySqlConnection(MyConnection);
                    //This is command class which will handle the query and connection object.
                    MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyCommand.Parameters.Add(new MySqlParameter("IMG", saveImage[0]));
                    MyReader = MyCommand.ExecuteReader(); 
                    while (MyReader.Read())
                    {
                    }
                    MyConn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }
        void ifExit(string ID)
        {
            MySqlConnection conn = new MySqlConnection(MyConnection);
            conn.Open();
            try
            {
                MySqlCommand command = new MySqlCommand("SELECT COUNT(*) FROM bpks.asm_registras_einamasis WHERE Asm_ID = '"+ ID +"' ;", conn);
                count = command.ExecuteScalar().ToString();
                conn.Close();
            }
            catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            if (count == "2")
            {
                try
                {
                    string Query = "DELETE FROM bpks.asm_registras_einamasis WHERE Asm_ID = '" + ID + "' ;";
                    MySqlConnection MyConn2 = new MySqlConnection(MyConnection);
                    MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                    MySqlDataReader MyReader2;
                    MyConn2.Open();
                    MyReader2 = MyCommand2.ExecuteReader();
                    while (MyReader2.Read())
                    {
                    }
                    MyConn2.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                count = "";
            }
            
        }
        void playSound(string soundName)
        {
            System.Media.SoundPlayer player = new System.Media.SoundPlayer(Application.StartupPath + "/Sound/" + soundName);
            player.Play();
        }
        
        //Display date/time to form
        private void timer1_Tick(object sender, EventArgs e)
        {
            time.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Set form maximum size
            this.MaximumSize = new Size(1263, 453);
            //Set form minimum size
            this.MinimumSize = new Size(1093, 453);
            //Check the database connection
            check_DB_conn = checkDBConnection();
            button_start_stop.Image = Image.FromFile(Application.StartupPath + "/Images/1432334245_control_pause_record.png");
            //Align the image and text on the button.
            button_start_stop.ImageAlign = ContentAlignment.MiddleRight;
            button_start_stop.TextAlign = ContentAlignment.MiddleLeft;
        }

        private void button_StartStop_Click(object sender, EventArgs e)
        {
            buttonClick++;
            if (buttonClick == 1) initialise_capture();
            if (buttonClick == 2)
            {
                stop_capture();
                buttonClick = 0;
            }

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            retrain();
            //Show last retrain time
            label_last_update.Text =  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

    }
}

