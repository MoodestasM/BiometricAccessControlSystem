﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing.Imaging;
using System.Drawing;
using MySql.Data.MySqlClient;
//EMGU Lib
using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

namespace BPKS_atpazinimo_programa
{

    class Recognize : IDisposable
    {
        //Global variables region
        #region Variables
        // Connection to database parameter
        //string MyConnection = "datasource=127.0.0.1;port=3306;username=root;password=Modestas";
        string MyConnection = "datasource=127.0.0.1;port=3306;username=root;password=Modestas";

        //ObjectRecognizer recognizer;
        FaceRecognizer recognizer;

        //For face images and ID
        List<Image<Gray, byte>> faceImages = new List<Image<Gray, byte>>();
        List<string> NamesList = new List<string>(); 
        List<int> NamesList_ID = new List<int>();
        float Distance = 0;
        string Recogn_Person_ID;
        //int Threshold = 2000;
        //Save value or person data is prepared
        bool _IsPrepared = false;
        #endregion

        public Recognize()
        {
            _IsPrepared = LoadPersonData();
        }
        //Prepare data
        public bool Prepare()
        {
            return _IsPrepared = LoadPersonData();
        }
 
        public bool IsTrained
        {
            get { return _IsPrepared; }
        }

        // Face Recognition method
        public string Recognise(Image<Gray, byte> Input_image)//, int Thresh = -1)
        {
            if (_IsPrepared)
            {
                //FaceRecognizer.PredictionResult Structure
                FaceRecognizer.PredictionResult ER = recognizer.Predict(Input_image);

                if (ER.Label == -1)
                {
                    Recogn_Person_ID = "unrecognized";
                   // Distance = 0;
                    return Recogn_Person_ID;
                }
                else
                {
                    Recogn_Person_ID = NamesList[ER.Label];
                    //Distance = (float)ER.Distance;
                    //if (Thresh > -1) Threshold = Thresh;
                    //Return the recognized person ID
                    return Recogn_Person_ID;     
                }
            }
            else return "";
        }
      
        /// Returns a float confidence value for potential false clasifications
        public float Get_Distance
        {
            get
            {
                //get Distance
                return Distance;
            }
        }
        /// Dispose of Class call Garbage Collector
        public void Dispose()
        {
            recognizer = null;
            faceImages = null;
            NamesList = null;
            GC.Collect();
        }

        private bool LoadPersonData()
        {
            NamesList.Clear();
            NamesList_ID.Clear();
            faceImages.Clear();
            try
            {
                try
                {
                    //This is my insert query in which i am taking input from the user through windows forms
                    //string Query = "select Asm_ID, BPKS_Nuotrauka from  bpks.asm_nuotraukos order by ID;";
                    string Query = "select ID, BPKS_Nuotrauka from  bpks.asm_nuotraukos order by ID;";
                    //This is  MySqlConnection here i have created the object and pass my connection string.
                    MySqlConnection MyConn = new MySqlConnection(MyConnection);
                    //This is command class which will handle the query and connection object.
                    MySqlCommand MyCommand = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyCommand.ExecuteReader();     //Here our query will be executed and data saved into the database.
                    while (MyReader.Read())
                    {
                        NamesList_ID.Add(NamesList.Count); //0, 1, 2, 3....
                        //Add Asm_ID value to name list
                        //NamesList.Add(MyReader.GetString("Asm_ID"));
                        NamesList.Add(MyReader.GetString("ID"));
                        string picdata = MyReader.GetString("BPKS_Nuotrauka");
                        byte[] picData = MyReader["BPKS_Nuotrauka"] as byte[] ?? null;
                        //Convert bit array to bitmap
                        ImageConverter pic = new ImageConverter();
                        Image img = (Image)pic.ConvertFrom(MyReader["BPKS_Nuotrauka"]);
                        Bitmap bmp = new Bitmap(img);
                        //Add  BPKS_Nuotrauka value to face list
                        faceImages.Add(new Image<Gray, byte>(bmp));
                    }
                    MyConn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                if (faceImages.ToArray().Length != 0)
                {
                    // LBPH Use 0 - Threshold value = Recognise  and > Threshold value = Unknown
                    // LBPHFaceRecognizer constructor with parameter
                    // default value threshold = 100
                    recognizer = new LBPHFaceRecognizer(1, 8, 8, 8, 70);
                    //FaceRecognizer train with faces form database
                    recognizer.Train(faceImages.ToArray(), NamesList_ID.ToArray());
                    //If data prepared return true
                    return true;
                }
                //Else return false
                else return false;
            }
            //If catched error
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}